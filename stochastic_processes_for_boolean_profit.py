from manimlib.imports import *
import numpy as np
import itertools

LIGHT_PINK = "#DC75CD"

# Humble beginnings:

# class Shapes(Scene):
#     #A few simple shapes
#     def construct(self):
#         circle = Circle()
#         square = Square()
#         line=Line(np.array([3,0,0]),np.array([5,0,0]))
#         triangle=Polygon(np.array([0,0,0]),np.array([1,1,0]),np.array([1,-1,0]))

#         self.add(line)
#         self.play(ShowCreation(circle))
#         self.play(FadeOut(circle))
#         self.play(GrowFromCenter(square))
#         self.play(Transform(square,triangle))

        
###############################################################
### Helper functions for animations and Boolean functions
###############################################################
        
def hamming_distance(x,y):
    return sum(i != j for i,j in zip(x,y))

def get_2d_gaussian_pdf(mux, muy, sigma):
    def f(u,v):
        x = v*np.cos(u)
        y = v*np.sin(u)
        r2 = (x-mux)*(x-mux) + (y-muy)*(y-muy)
        return np.array([x,y,10*(1/(sigma*2*np.pi))*np.exp(-0.5*(r2/sigma)**2)])
    
    return f

def generate_brownian_motion_until_condition(initial_position, step_size, predicate, dimensions, should_save_path=True):
    positions = [initial_position]
    while not predicate(positions[-1]):
        next_position = positions[-1] + np.random.normal(0, step_size, dimensions)
        if should_save_path:
            positions.append(next_position)
        else:
            positions = [next_position]
            
    return np.array(positions)

def interpolate_path(path, min_t, max_t):
    x_points = np.linspace(min_t, max_t, len(path))
    def f(x):
        return np.interp(x, x_points, path)
    return f

def get_b_path_complete():
    """
    A path of a B_i parameter, as defined in the paper. (A martingale which has |b(t)| = t, and which flips signs in a martingale-like fashion.
    """
    initial_position = np.zeros(1);
    step_size = 0.01
    bound = 1
    path = generate_brownian_motion_until_condition(initial_position, step_size, lambda x: any(abs(x) > bound), 1)

    current_best = -1
    points_of_increased_abs_value = []
    for p in path:
        if abs(p) > current_best:
            points_of_increased_abs_value.append(p)
            current_best = abs(p)

    points_of_increased_abs_value = np.array(points_of_increased_abs_value)
    sign_changes = (np.sign(points_of_increased_abs_value[:-1]) != np.sign(points_of_increased_abs_value[1:])).nonzero()[0]    
    ## We almost surely change sign after the step, since sign0 = 0.
    sign_changes = sign_changes[1:]

    initial_sign = np.sign(points_of_increased_abs_value[1])
    values_of_sign_change = points_of_increased_abs_value[sign_changes]

    threshold_values = 0.5*(abs(points_of_increased_abs_value[sign_changes]) +abs( points_of_increased_abs_value[sign_changes+1]))
    threshold_values = np.append(threshold_values, 1.1)

    def b(t):
        if len(threshold_values) == 0:
            return t*initial_sign    

        i = np.argmax(threshold_values > t)
        return initial_sign*t*((-1)**(i))

    return np.vectorize(b), path, values_of_sign_change, points_of_increased_abs_value

def get_b_path():
    return get_b_path_complete()[0]


def get_B_path(n):
    paths = [get_b_path() for i in range(n)]    
    
    def B(t):
        return np.array([p(t) for p in paths]).transpose()
    return B


def get_quadratic_variation(f_t, B, n):
    """
    Returns the quadratic variation function, given both the function f_t 
    and the underlying process B (B is used for detecting jumps).
    """
    discontinuities = []
    dt = 1/2000
    for t in np.linspace(2*dt, 1-dt, 1000):
        bminus = B(t-dt)[0]
        bplus = B(t+dt)[0]
        for i in range(n):
            if np.sign(bminus[i]) != np.sign(bplus[i]):
                discontinuities.append(t)
                break
            
    def quadvar(t):
        relevant_times = [s for s in discontinuities if s <= t]
        return sum([(f_t(s+dt) - f_t(s-dt))**2 for s in relevant_times])

    return quadvar


def fix_bit(f, bit_index, value):
    """
    Fixes the i-th bit of a Boolean function to a preset value (aka restriction)
    """
    def fixed_bit_f(y):
        y = list(y)
        y[bit_index] = value;
        return f(y)
    return fixed_bit_f


def calculate_expectation(f, n):
    """
    Calculates the expectation of a Boolean function on n bits.
    """
    return np.mean(np.array([f(y) for y in itertools.product([-1,1], repeat=n)]))        


def get_derivative(f, i):
    def df(y):
        y_plus = np.copy(y)
        y_plus[i] = 1
        y_minus = np.copy(y)
        y_minus[i] = -1
        return 0.5*(f(y_plus) - f(y_minus))

    return df


def majority(y):
    """
    The majority function on any number of bits, assuming input is in {-1,1}^n.
    """
    return 1 if sum(y) > 0 else -1


def nand(y):
    """
    Nand function on two bits.
    """
    if y[0] == 1 and y[1] == 1:
        return -1
    else:
        return 1


def get_iterated_function(function, depth, split_size):
    """
    Functions will implicitly assume that n is a power of split_size.
    """
    if depth == 1:
        return function
    next_level = get_iterated_function(function, depth-1, split_size)
    def f(x):
        n = len(x)
        return function([next_level(x[i*n//split_size:(i+1)*n//split_size]) for i in range(split_size)])
    return f


def get_harmonic_weights(y):
    y = np.array(y)
    def w(x):
        x = np.array(x)
        if len(x.shape) == 1:
            x = np.reshape(x,(1,-1))
        return np.prod(0.5*(1+x*y),1)
    return w


def get_harmonic_extension(f, n):

    weights = [get_harmonic_weights(y) for y in itertools.product([-1,1], repeat=n)]
    values = np.array([f(y) for y in itertools.product([-1,1], repeat=n)])
    
    def harmonic_extension(x):
        return np.dot(np.array([w(x) for w in weights]).transpose(),values)

    return harmonic_extension


###############################################################
### Helper classes
###############################################################

class Hypercube:
    #TODO: Make this a mobject, and its various draw-ables submobjects.
    """
    The 4 dimensional hypercube, right here in your own 2d screen. 
    It's actually two 3d cubes, one large, one small, joined together,
    then obliquely projected after rotation. 
    """
    def __init__(self, shift=None, scale=1):
        if shift is None:
            shift = np.zeros(3)
        self.shift = shift
        self.scale = scale
        
        outer_cube = [np.array(x) for x in itertools.product([-1,1], repeat=3)]
        inner_cube = [np.array(x) for x in itertools.product([-0.5,0.5], repeat=3)]
        cube = outer_cube + inner_cube
        
        rotation_angle = 0.18 * np.pi
        rotation_matrix = np.array([[np.cos(rotation_angle), 0, np.sin(rotation_angle)],
                                   [0,1,0],
                                    [-np.sin(rotation_angle), 0, np.cos(rotation_angle)]])

        tilt_angle = 0.3*np.pi
        tilt_matrix = np.array([[1, 0, 0],
                                [0, 1, 0.5*np.sin(tilt_angle)],
                                [0,0,1]])

        scale_matrix = scale * 0.9 * np.array([[1.3, 0, 0],
                                [0, 1, 0],
                                [0,0,1.3]])


        cube = [tuple(v) for v in cube]
        outer_cube = [tuple(v) for v in outer_cube]
        inner_cube = [tuple(v) for v in inner_cube]
        self.transformation_matrix = np.dot(tilt_matrix, np.dot(rotation_matrix, scale_matrix))
        
        list_of_edges = [(v1,v2) for v1,v2 in itertools.combinations(outer_cube,2) if hamming_distance(v1,v2) == 1]
        list_of_edges += [(v1,v2) for v1,v2 in itertools.combinations(inner_cube, 2) if hamming_distance(v1,v2) == 1]
        list_of_edges += [(v1,v2) for v1,v2 in itertools.product(outer_cube, inner_cube) if hamming_distance(np.sign(v1), np.sign(v2)) == 0]

        self.nodes_to_draw = {v: self.create_node_to_draw_from_node(v) for v in cube}
        self.edges_to_draw = {e: self.create_edge_to_draw_from_nodes(e[0],e[1]) for e in list_of_edges}
        self.nodes = cube
        self.edges = list_of_edges

    def get_neighbors(self, source):
        return [v for v in self.nodes if hamming_distance(source, v) == 1 or (hamming_distance([np.sign(x) for x in source], [np.sign(x) for x in v]) == 0 and abs(source[0]) != abs(v[0]))]

    def get_sorted_drawing(self):
        sorted_nodes = sorted(self.nodes_to_draw.values(), key=lambda x: x.get_z(), reverse=True)
        return list(self.edges_to_draw.values()) + sorted_nodes

    def get_pivotal_edges(self, function_indicator):
        return [(v1,v2) for (v1,v2) in self.edges if (v1 in function_indicator and v2 not in function_indicator) or (v2 in function_indicator and v1 not in function_indicator)]

    def create_node_to_draw_from_node(self, v):
        screen_coordinate = np.dot(self.transformation_matrix, v) + self.shift
        fill_color = DARK_GRAY        
        p = Circle(color=GOLD_A, fill_color=fill_color, fill_opacity=1.0)
        p.scale(self.scale * 0.2)
        p.shift(screen_coordinate)
        return p

    def create_edge_to_draw_from_nodes(self, v1, v2):
        screen_coordinate1 = np.dot(self.transformation_matrix, v1) + self.shift
        screen_coordinate2 = np.dot(self.transformation_matrix, v2) + self.shift

        a = abs(v1[0])
        b = abs(v2[0])
        if (a,b) == (1,1):
            color = GOLD_B
        if (a,b) == (0.5,0.5):
            color = TEAL_A
        if (a,b) == (1, 0.5) or (a,b) == (0.5, 1):
            color = PURPLE_A
        
        return Line(screen_coordinate1, screen_coordinate2, color=color)


class BinaryInput:
    #TODO: Make this a mobject, and its various draw-ables submobjects.
    
    def __init__(self, num_of_vars, length=2, shift=None):
        if shift is None:
            shift = np.zeros(3)
            
        self.group = VGroup()
        hline = Line(length*LEFT, length*RIGHT)
        self.group.add(hline)
        self.lvlines_for_digits = []
            
        line_positions = np.linspace(-length, length, num_of_vars+1)
        for x in line_positions:
            lvline = Line(x*RIGHT + 0.5* UP, x*RIGHT)
            self.lvlines_for_digits.append(lvline)
            self.group.add(lvline)

        self.lvlines_for_digits = self.lvlines_for_digits[:-1]
        self.digits_to_draw = [None]*num_of_vars
        
        self.group.shift(shift)

    def set_digits_to_draw(self, digits):
        for i,d in enumerate(digits):
            self.set_digit_to_draw(i,d)

    def set_digit_to_draw(self, i, digit):
        """
        Set the i-th digit to be digit (a single string character)
        """
        digit_offset = 0.5*(self.lvlines_for_digits[1].get_x() - self.lvlines_for_digits[0].get_x())

        l = self.lvlines_for_digits[i]
        t = TexMobject(digit)
        t.set_color(YELLOW)
        t.scale(0.8)
        t.next_to(l,0)
        t.shift(digit_offset*RIGHT)
        self.digits_to_draw[i] = t
    
class HypercubeScene(Scene):
    """
    Helps with the animation of hypercubes. Has no rightful existence, 
    basically just wanted a place to put some drawing functions.
    (the hypercube isn't a member, since scenes may contain more than 
    one hypercube anyway)
    """

    def highlight_edge(self, h, v1, v2, speed_factor=1, should_return_to_original_nodes=True, should_return_to_original_edges=True):
        self.highlight_edges(h, [v1], [v2], speed_factor, should_return_to_original_nodes, should_return_to_original_edges)

    def highlight_edges_from_source(self, h, v1, targets, speed_factor=1, should_return_to_original_nodes=True, should_return_to_original_edges=True):
        self.highlight_edges(h, [v1]*len(targets), targets, speed_factor, should_return_to_original_nodes, should_return_to_original_edges)

    def highlight_edges(self, h, start_vertices, end_vertices, speed_factor=1, should_return_to_original_nodes=True, should_return_to_original_edges=True):
        start_to_draw_all = [h.nodes_to_draw[v1] for v1 in start_vertices]
        end_to_draw_all = [h.nodes_to_draw[v2] for v2 in end_vertices]

        edges_to_draw = [h.edges_to_draw[v1,v2] if (v1,v2) in h.edges_to_draw else h.edges_to_draw[v2,v1] for v1,v2 in zip(start_vertices, end_vertices)]

        start_fill_color = [start_to_draw.get_fill_color() for start_to_draw in start_to_draw_all]
        start_stroke_color = [start_to_draw.get_stroke_color() for start_to_draw in start_to_draw_all]
        start_stroke_width = [start_to_draw.get_stroke_width() for start_to_draw in start_to_draw_all]
        end_stroke_color = [end_to_draw.get_stroke_color() for end_to_draw in end_to_draw_all]
        end_stroke_width = [end_to_draw.get_stroke_width() for end_to_draw in end_to_draw_all]
        edge_colors = [edge_to_draw.get_color() for edge_to_draw in edges_to_draw]
        edge_widths = [edge_to_draw.get_stroke_width() for edge_to_draw in edges_to_draw]    

        fill_animation1 = list(itertools.chain.from_iterable((start_to_draw.set_fill, RED) for start_to_draw in start_to_draw_all))
        self.play(*fill_animation1, run_time=0.5*speed_factor)
        edge_animation = list(itertools.chain.from_iterable((edge_to_draw.set_stroke, BLUE, 8) for edge_to_draw in edges_to_draw))
        self.play(*edge_animation, run_time=0.5*speed_factor)
        self.wait(0.1*speed_factor)
        for start_to_draw, fill_color in zip(start_to_draw_all, start_fill_color):
            start_to_draw.set_fill(color=fill_color)
        node_animations = list(itertools.chain.from_iterable((node_to_draw.set_stroke, BLUE, 8) for node_to_draw in start_to_draw_all)) + list(itertools.chain.from_iterable((node_to_draw.set_stroke, BLUE, 10) for node_to_draw in end_to_draw_all))
        self.play(*node_animations, run_time=0.5*speed_factor)
        self.wait(1.2*speed_factor)

        if should_return_to_original_nodes:
            for node_to_draw, node_width, node_stroke_color in zip(start_to_draw_all, start_stroke_width, start_stroke_color):
                node_to_draw.set_stroke(color=node_stroke_color, width=node_width)

            for node_to_draw, node_width, node_stroke_color in zip(end_to_draw_all, end_stroke_width, end_stroke_color):
                node_to_draw.set_stroke(color=node_stroke_color, width=node_width)

        if should_return_to_original_edges:
            for edge_to_draw, edge_width, edge_color  in zip(edges_to_draw, edge_widths, edge_colors):
                edge_to_draw.set_stroke(width=edge_width, color=edge_color)


class ShowSegmentOfCreation(ShowPartial):
    CONFIG = {
        "lag_ratio": 1,
    }

    def __init__(self, vmobject, start_alpha, end_alpha, **kwargs):
        self.start_alpha = start_alpha
        self.end_alpha = end_alpha
        super().__init__(vmobject, **kwargs)

    def get_bounds(self, alpha):
        return (min(alpha, self.start_alpha), min(alpha, self.end_alpha))


###############################################################
### The actual scenes
###############################################################

class Intro(Scene):
    def construct(self):
        circle_animation_time = 0.1
        wait_time = 0.1

        h = Hypercube()
        white_vertices = [(1,1,1), (1,-1,1), (-1,-1,1), (-0.5, -0.5, 0.5),
                          (-0.5, -0.5, -0.5), (-0.5, 0.5, -0.5), (0.5, 0.5, -0.5), (1,1,-1)]
        black_vertices = [(-1,1,-1), (-1,-1,-1), (1,-1,-1), (0.5,-0.5,-0.5),
                        (0.5,-0.5,0.5), (0.5,0.5,0.5), (-0.5,0.5,0.5), (-1,1,1)]
        for v in white_vertices:
            h.nodes_to_draw[v].set_fill(color=WHITE)
        
        for (x,y,z) in black_vertices:
            p = h.nodes_to_draw[(x,y,z)]
            self.play(ShowCreation(p, run_time=circle_animation_time))
        self.wait(wait_time)

        for (x,y,z) in white_vertices:
            p = h.nodes_to_draw[(x,y,z)]
            self.play(ShowCreation(p, run_time=circle_animation_time))
        self.wait(wait_time)

        self.play(*[GrowFromCenter(e) for e in h.edges_to_draw.values()])
        self.wait(0.2)

        # self.add(*h.nodes_to_draw.values())
        # self.add(*h.edges_to_draw.values())
        
        title1 = TextMobject("Concentration on the Boolean hypercube")
        title2 = TextMobject("via pathwise stochastic analysis")
        title1.shift(2.2*DOWN + 5.7 * UP)
        title2.shift(3.0*DOWN + 5.7 * UP)
        
        ronen_text = TextMobject("Ronen Eldan")
        renan_text = TextMobject("Renan Gross")
        weizmann_text = TextMobject("Weizmann Institute of Science")
        ronen_text.shift(2.3* DOWN + 3* LEFT)
        renan_text.shift(2.3* DOWN + 3*RIGHT)
        weizmann_text.shift(3 * DOWN)
        
        self.play(*[DrawBorderThenFill(t, run_time=1.5) for t in [title1, title2, ronen_text, renan_text, weizmann_text]])
        
        self.wait(15)


class OverviewScene(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : -1,
        "y_max" : 1,
        "x_axis_width" : 3*FRAME_WIDTH / 4,
        "y_axis_height": 3*FRAME_HEIGHT / 4,
        "graph_origin" : 3*FRAME_WIDTH/8 * LEFT,
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "x_axis_label": "$t$",
    }
        
    def construct(self):
        # self.wait(3)
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)        
        ##############################
        ## Some nice cubes
        ##############################
        t1 = TexMobject(r"\mathrm{Var}[f]", r"= \mathcal{O}(1 + \mathbb{E}f)")
        t1.shift(3.7*LEFT + 3*UP)
        t1.set_color_by_tex("Var", RED_C)

        t2 = TexMobject(r"\mathrm{Inf}(f) = ", r"\sum_{i=1}^n \mathrm{Inf}_i(f)")
        t2.set_color_by_tex(r"\sum_{i=1}^n \mathrm{Inf}_i(f)", BLUE_C)
        t2.shift(3.5*RIGHT + 3*UP)

        self.add(t1, t2)
        
        h1 = Hypercube(shift=3.5*LEFT + DOWN)
        random.seed(55)
        function_indicator1 = random.sample(h1.nodes, 8)
        function_nodes = [h1.nodes_to_draw[v] for v in function_indicator1]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

            
        h2 = Hypercube(shift=3.5*RIGHT + DOWN)
        random.seed(55)
        function_indicator2 = random.sample(h2.nodes, 6)
        function_nodes = [h2.nodes_to_draw[v] for v in function_indicator2]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

            
        self.play(*[GrowFromCenter(x) for x in h1.get_sorted_drawing()], *[GrowFromCenter(x) for x in h2.get_sorted_drawing()])
            
        animations1 = list(itertools.chain.from_iterable([(h1.nodes_to_draw[v].set_stroke, RED, 10) for v in function_indicator1]))

        pivotal_edges = h2.get_pivotal_edges(function_indicator2)
        dir1 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[1] == v2[1] and v1[2] == v2[2]]
        dir2 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] == v2[0] and v1[2] == v2[2]]
        dir3 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] == v2[0] and v1[1] == v2[1]]
        dir4 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] != v2[0] and v1[1] != v2[1] and v1[2] != v2[2]]

        animations2 = list(itertools.chain.from_iterable([(h2.edges_to_draw[e].set_stroke, BLUE, 15) for e in dir1]))

        self.play(*animations1, *animations2)
        
        directions = [dir2, dir3, dir4]
        for direction in directions:            
            animations = list(itertools.chain.from_iterable([(h2.edges_to_draw[e].set_stroke, BLUE, 15) for e in direction]))
            self.play(*animations)
        self.wait(3)


        ##############################
        ### The diamond sneak peek
        ##############################

        black_screen = Rectangle(fill_opacity=1, fill_color=BLACK, stroke_color=BLACK)
        black_screen.scale(10)
        self.add(black_screen)
        
        poincare_text = TexMobject(r"\mathrm{Var}[f]", r"\leq", r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        kkl_text =TexMobject(r"\mathrm{Var}[f]", r"\leq C", r"{\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", r"\log(1/ \max_i \mathrm{Inf}_i(f))}")
        tal_text = TexMobject(r"\mathrm{Var}[f]", r"\leq C", r"\mathbb{E} \sqrt{h_f}")

        poincare_text.set_color_by_tex("Var", RED_C)
        poincare_text.set_color_by_tex("Inf", BLUE_C)
        poincare_text.shift(2.9*UP)

        kkl_text.set_color_by_tex("Var", RED_C)
        kkl_text.set_color_by_tex("\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
        kkl_text.set_color_by_tex("log", GREEN_C)
        kkl_text.shift(4*LEFT)
        kkl_text.scale(0.8)

        kkl2 = TextMobject("KKL")
        kkl2.align_to(kkl_text, LEFT)
        kkl2.shift(UP)

        tal_text.set_color_by_tex("Var", RED_C)
        tal_text.set_color_by_tex("sqrt", YELLOW_C)
        tal_text.shift(4*RIGHT)

        tal2 = TextMobject("Talagrand")
        tal2.align_to(tal_text, RIGHT)
        tal2.shift(UP)

        l1 = Arrow()
        l1.put_start_and_end_on(kkl_text.get_edge_center(UP), poincare_text.get_edge_center(DOWN))
        l1.scale(0.6)
        l1.set_color(GRAY)

        l2 = Arrow()
        l2.put_start_and_end_on(tal_text.get_edge_center(UP), poincare_text.get_edge_center(DOWN))
        l2.scale(0.6)
        l2.set_color(GRAY)

        tal_conj = TexMobject(r"\mathrm{Var}[f]",
                              r"\sqrt{\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)}",
                              r"\leq C", r"\mathbb{E}\sqrt{h_f}")
        tal_conj.set_color_by_tex(r"\mathbb{E}\sqrt{h_f}", YELLOW_C)
        tal_conj.set_color_by_tex(r"Var", RED_C)
        tal_conj.set_color_by_tex(r"Inf", GREEN_C)
        tal_conj.shift(3.*DOWN)
        tal_conj.scale(0.9)

        l3 = Arrow()
        l3.put_start_and_end_on(tal_conj.get_edge_center(UP), kkl_text.get_edge_center(DOWN))
        l3.scale(0.6)
        l3.set_color(GRAY)

        l4 = Arrow()
        l4.put_start_and_end_on(tal_conj.get_edge_center(UP), tal_text.get_edge_center(DOWN))
        l4.scale(0.6)
        l4.set_color(GRAY)

        mystery = TextMobject("???")
        mystery.shift(tal_conj.get_center())
        
        conj_text = TextMobject("Conj:")
        conj_text.next_to(tal_conj, LEFT)
        conj_text.align_to(kkl2, LEFT)

        thm_text = TextMobject("EG20:")
        thm_text.next_to(tal_conj, LEFT)
        thm_text.align_to(kkl2, LEFT)
        thm_text.shift(0.05*UP)

        r = Rectangle()
        r.stretch(3.4, 0)
        r.stretch(0.9, 1)
        r.align_to(thm_text, LEFT)
        r.align_to(tal_conj, DOWN)
        r.shift(0.2*LEFT + 0.2*DOWN)
        r.set_color(GOLD)
        r.set_stroke(width=10)

        self.add(poincare_text, kkl_text, kkl2, tal_text, tal2)
        self.wait(1)
        self.play(ShowCreation(l1))
        self.play(ShowCreation(l2))
        self.play(ShowCreation(l3), ShowCreation(l4))
        self.wait(0.2)
        self.play(Write(mystery))
        self.wait(3)        
     
        #############################
        ## Some nice Brownian motion
        ############################# 

        black_screen = Rectangle(fill_opacity=1, fill_color=BLACK, stroke_color=BLACK)
        black_screen.scale(10)
        self.add(black_screen)

        self.setup_axes(animate=False)
        
        np.random.seed(167)
        number_of_functions_to_show = 1
        for i in range(number_of_functions_to_show):
            b, path, sign_change, increased_abs_value = get_b_path_complete()
            path = np.array([x[0] for x in path])
            increased_abs_value = np.array([x[0] for x in increased_abs_value])

            only_positive_increase_path = np.array([x if x in increased_abs_value and x > 0 else 0 for x in path])
            for i in range(1,len(only_positive_increase_path)):
                if only_positive_increase_path[i] == 0:
                    only_positive_increase_path[i] = only_positive_increase_path[i-1]

            only_negative_increase_path = np.array([x if x in increased_abs_value and x < 0 else 0 for x in path])
            for i in range(1,len(only_negative_increase_path)):
                if only_negative_increase_path[i] == 0:
                    only_negative_increase_path[i] = only_negative_increase_path[i-1]

            brownian_motion = interpolate_path(path, 0, 1)
            g_brownian = self.get_graph(brownian_motion, step_size=0.001, color=YELLOW)
            g_brownian.make_jagged()

            fp = interpolate_path(only_positive_increase_path, 0, 1)
            g_positive = self.get_graph(fp, step_size=0.001, color=GREEN)
            g_positive.make_jagged()

            fn = interpolate_path(only_negative_increase_path, 0, 1)
            g_negative = self.get_graph(fn, step_size=0.001, color=RED)
            g_negative.make_jagged()

            g_martingale = self.get_graph(b, step_size=0.001, color=YELLOW)
            g_martingale.make_jagged()

            self.play(ShowCreation(g_brownian), ShowCreation(g_positive), ShowCreation(g_negative), run_time=4, rate_func=linear)

            self.wait(2)
            self.play(ReplacementTransform(g_brownian, g_martingale), ReplacementTransform(g_positive, g_martingale), ReplacementTransform(g_negative, g_martingale))
            self.wait(2)
            self.remove(g_martingale)


        ##################################
        ## The 3d martingale path
        ##################################
        black_screen = Rectangle(fill_opacity=1, fill_color=BLACK, stroke_color=BLACK)
        black_screen.scale(10)
        self.add(black_screen)
        
        n = 3
        
        h = Hypercube(scale=2)
        m = h.transformation_matrix
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK, GRAY, WHITE]

        np.random.seed(434) # other candidates: 425, 431, 432
        functions  = []
        number_of_functions_to_show = 7
        for i in range(number_of_functions_to_show):

            B = get_B_path(n)
            b = get_b_path() # Shit! I forgot this here and based my seed choice with it. It must remain...

            if i == 5: ## Don't want the 6th one here, not very pretty
                continue

            f = lambda t: np.dot(m, B(t)[0])
            g = ParametricFunction(f, x_min=0, x_max=1, step_size=0.005, color=colors[i])
            g.set_stroke(width=7)
            g.make_jagged()
            functions.append(g)

        # Draw just the outer cube
        outer_cube = [np.array(x) for x in itertools.product([-1,1], repeat=3)]
        outer_cube = [tuple(v) for v in outer_cube]              
        outer_edges = [(v1,v2) for v1,v2 in itertools.combinations(outer_cube,2) if hamming_distance(v1,v2) == 1]
        to_draw = [h.edges_to_draw[v1,v2] for v1,v2 in outer_edges]
        for d in to_draw:
            d.set_stroke(color=GRAY)
            
        # self.play(*[GrowFromCenter(d) for d in to_draw])
        self.add(*to_draw)
        for g in functions:
            self.play(ShowCreation(g), run_time=1.1, rate_func=linear)

        self.wait(10)

        
class FunctionDefinitionScene(Scene):
    def construct(self):

        bf1 = TexMobject(r"f : \{-1,1\}^n \to")
        bf2 = TexMobject(r"\{-1,1\}")

        bf2.next_to(bf1, RIGHT)
        v = VGroup(bf1, bf2)

        v.shift(3*LEFT + 3*UP)
        self.play(Write(bf1))
        self.wait(2)
        self.play(Write(bf2))        
        self.wait(2)

        h1 = Hypercube(shift=3.5*RIGHT + 1*UP, scale = 0.8)
        to_draw = h1.get_sorted_drawing()
        self.play(*[GrowFromCenter(x) for x in to_draw])
        # self.add(*to_draw)
        
        self.wait(2)

        dictator_nodes = [h1.nodes_to_draw[v] for v in h1.nodes if v[0] >= 0]
        dictator_animation = list(itertools.chain.from_iterable((node_to_draw.set_fill, WHITE) for node_to_draw in dictator_nodes))
        self.play(*dictator_animation)
        
        dictator_text = TexMobject(r"\text{Dictator}(x) = x_1")
        dictator_text.shift(3*LEFT + 1 * UP)
        self.play(Write(dictator_text, run_time=0.4))

        self.wait(2)

        h2 = Hypercube(shift=3.5*RIGHT + 2*DOWN, scale = 0.8)
        to_draw = h2.get_sorted_drawing()
        self.play(*[GrowFromCenter(x) for x in to_draw])
        # self.add(*to_draw)

        parity_nodes = [h2.nodes_to_draw[v] for v in h2.nodes if np.prod(v) == 1 or np.prod(v) == -0.5**3]
        parity_animation = list(itertools.chain.from_iterable((node_to_draw.set_fill, WHITE) for node_to_draw in parity_nodes))
        self.play(*parity_animation)

        parity_text = TexMobject(r"\text{Parity}(x) = x_1 \cdot \ldots \cdot x_n")
        parity_text.shift(3*LEFT + 2*DOWN)
        self.play(Write(parity_text, run_time=0.4))
        
        self.wait(4)

        
class TwoMainPropertiesScene(HypercubeScene):
    def construct(self):

        dit1 = TexMobject(r"\mathrm{Inf}_1(\mathrm{Dictator}) = 1")
        dit1.shift(3*LEFT + 1*UP)
        
        dit2 = TexMobject(r"\mathrm{Inf}_2(\mathrm{Dictator}) = 0")
        dit2.shift(3*LEFT )
        
        inf_text = TexMobject(r"\mathrm{Inf}_i(f)", r"= \underset{x \sim \{-1,1\}^n}{\mathbb{P}}[f(x) \neq f(x^{\oplus i})]")
        inf_text.shift(4*LEFT + 3*UP)
        inf_text.align_to(dit1, LEFT)
        
        self.play(Write(inf_text))

        self.wait(1)
        
        dit1.shift(0.5*RIGHT)
        dit2.shift(0.5*RIGHT)        

        h1 = Hypercube(shift=3.5*RIGHT + 0.3*UP)
        to_draw = h1.get_sorted_drawing()

        dictator_nodes = [h1.nodes_to_draw[v] for v in h1.nodes if v[0] >= 0]
        for node_to_draw in dictator_nodes:
            node_to_draw.set_fill(color=WHITE)
        
        self.play(*[GrowFromCenter(x) for x in to_draw])
        self.add(*to_draw)

        random.seed(656)
        number_of_random = 5
        random_nodes = random.sample(h1.nodes, number_of_random)

        for i,v1 in enumerate(random_nodes):
            v2 = list(v1)
            v2[0] = -v2[0]
            v2 = tuple(v2)
            self.highlight_edge(h1, v1, v2, speed_factor=0.5)


        self.play(Write(dit1, run_time=0.4))
        self.wait(2)
            
        for i,v1 in enumerate(random_nodes):
            v2 = list(v1)
            v2[1] = -v2[1]
            v2 = tuple(v2)
            self.highlight_edge(h1, v1, v2, speed_factor=0.5)

        self.play(Write(dit2, run_time=0.4))
        self.wait(2)
        
        var_text1 = TexMobject(r"\mathrm{Var}[f] = ") 
        var_text2 = TexMobject(r"\mathbb{E}f^2")
        var_text3 = TexMobject(r"- \left(\mathbb{E}f\right)^2")
        one = TexMobject(r"1")
        var_text1.shift(3*LEFT + 2*DOWN)
        # var_text1.align_to(dit1, LEFT)
        var_text1.align_to(inf_text, LEFT)
        var_text2.next_to(var_text1, RIGHT)
        var_text3.next_to(var_text2, RIGHT)
        one.shift(var_text2.get_x()*RIGHT + var_text2.get_y()*UP)
        self.play(Write(var_text1, run_time=0.4))
        self.play(Write(var_text2, run_time=0.4))
        self.play(Write(var_text3, run_time=0.4))
        self.wait(1)
        self.play(Transform(var_text2, one))
        self.play(var_text3.next_to, var_text2, RIGHT)
        self.wait(5)

class GeometricInterpretationScene(Scene):
    def construct(self):
        vline = Line(4*UP, 4*DOWN)
        self.play(ShowCreation(vline))

        #########################################
        ## Explaining about variance 
        #########################################
        
        var_text1 = TexMobject(r"\mathrm{Var}[f]")
        var_text_eq = TexMobject(r"=")
        var_text_approx = TexMobject(r"=")
        var_text2 = TexMobject(r"1 - (\mathbb{E}f)^2")
        var_text22 = TexMobject(r"1 - (\mathbb{E}f)^2")
        var_text3 = TexMobject(r"(1 - \mathbb{E}f)")
        var_text4 = TexMobject(r"(1 + \mathbb{E}f)")
        var_text5 = TexMobject(r"(\mathbb{E}f \leq 0)")
        var_text5.scale(0.75)
        var_text6 = TexMobject(r"\mathcal{O}")
        
        var_text1.shift(5.5*LEFT + 3*UP)
        var_text1.set_color(RED_C)
        var_text_eq.next_to(var_text1, RIGHT)
        var_text_approx.next_to(var_text1, RIGHT)
        var_text2.next_to(var_text_eq, RIGHT)
        var_text22.next_to(var_text_eq, RIGHT)         
        var_text3.next_to(var_text_eq, RIGHT)
        var_text4.next_to(var_text3, RIGHT)
        var_text5.next_to(var_text3, UP)
        var_text5.set_color(GOLD_C)
        var_text6.next_to(var_text_eq, RIGHT)

        h1 = Hypercube(shift=3.5*LEFT + DOWN)
        to_draw = h1.get_sorted_drawing()
        random.seed(55)
        function_indicator = random.sample(h1.nodes, 8)
        function_nodes = [h1.nodes_to_draw[v] for v in function_indicator]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

        self.play(Write(var_text1), Write(var_text_eq), Write(var_text2), Write(var_text22), *[GrowFromCenter(x) for x in to_draw])
        self.wait(1)
        self.play(Transform(var_text2, var_text3), Transform(var_text22, var_text4))
        self.wait(1)
        self.remove(var_text3, var_text4)
        self.wait(1)
        self.play(Write(var_text5))
        self.wait(1)
        self.play(Transform(var_text2, var_text6), Transform(var_text_eq, var_text_approx), var_text22.next_to, var_text6, RIGHT)
        self.wait(1)
        animations = list(itertools.chain.from_iterable([(h1.nodes_to_draw[v].set_stroke, RED, 10) for v in function_indicator]))
        self.play(*animations)

        self.wait(1)
        
        #########################################
        ## Explaining about influence
        #########################################

        influence_text = TexMobject(r"\mathrm{Inf}(f) = ", r"\sum_{i=1}^n \mathrm{Inf}_i(f)")
        influence_text.set_color_by_tex(r"\sum_{i=1}^n \mathrm{Inf}_i(f)", BLUE_C)

        influence_text.shift(3.5*RIGHT + 3*UP)

        h2 = Hypercube(shift=3.5*RIGHT + DOWN)
        to_draw = h2.get_sorted_drawing()

        random.seed(55)
        function_indicator = random.sample(h2.nodes, 6)
        function_nodes = [h2.nodes_to_draw[v] for v in function_indicator]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

        self.play(Write(influence_text), *[GrowFromCenter(x) for x in to_draw], run_time = 1)
        self.wait(2)
        pivotal_edges = h2.get_pivotal_edges(function_indicator)
        dir1 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[1] == v2[1] and v1[2] == v2[2]]
        dir2 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] == v2[0] and v1[2] == v2[2]]
        dir3 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] == v2[0] and v1[1] == v2[1]]
        dir4 = [(v1,v2) for (v1,v2) in pivotal_edges if v1[0] != v2[0] and v1[1] != v2[1] and v1[2] != v2[2]]
        
        directions = [dir1, dir2, dir3, dir4]
        for direction in directions:            
            animations = list(itertools.chain.from_iterable([(h2.edges_to_draw[e].set_stroke, BLUE, 15) for e in direction]))
            self.play(*animations)
    
        self.wait(5)
        
class PoincareInequalityScene(ThreeDScene):

    def construct(self):
        poincare_title = TexMobject(r"\text{Poincar\'e Inequality}")        
        poincare_inequality = TexMobject(r"\mathrm{Var}[f]", r"\leq", r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        poincare_continuous = TexMobject(r"\mathrm{Var}[f]", r"\leq C", r"\int  || \nabla f||^2 d\mu")

        poincare_inequality.set_color_by_tex("Var", RED_C)
        poincare_inequality.set_color_by_tex("Inf", BLUE_C)
        
        total1 = VGroup(poincare_title, poincare_continuous)
        total1.arrange_submobjects(DOWN, buff=MED_LARGE_BUFF)

        total2 = VGroup(poincare_title, poincare_inequality)
        total2.arrange_submobjects(DOWN, buff=MED_LARGE_BUFF)

        poincare_title.shift(2*UP)
        poincare_inequality.shift(2*UP)
        poincare_continuous.shift(2*UP)

        
        self.play(Write(total1))    

        sphere = Sphere()
        sphere.shift(3*LEFT + 2*DOWN)
        sphere.scale(1.5)
        sphere.rotate_in_place(0.3*PI, axis = OUT + LEFT + UP)

        self.play(GrowFromCenter(sphere))
        
        param_max = 1.5
        gaussian = ParametricSurface(func=get_2d_gaussian_pdf(0,0,1), u_min=-PI, u_max=PI, v_min=0, v_max=3, checkerboard_colors=[RED_A, RED_B],)
        gaussian.rotate_in_place(0.45*PI, axis = LEFT)
        gaussian.shift(3*RIGHT + 1.5*DOWN)

        self.play(GrowFromCenter(gaussian))

        self.wait(1)

        h = Hypercube(shift=1.6*DOWN)
        to_draw = h.get_sorted_drawing()
        parity_nodes = [h.nodes_to_draw[v] for v in h.nodes if np.prod(v) == 1 or np.prod(v) == -0.5**3]
        for node_to_draw in parity_nodes:
            node_to_draw.set_fill(color=WHITE)

        cube = VGroup(*h.get_sorted_drawing())
        self.play(Transform(sphere, cube), Transform(gaussian, cube), Transform(poincare_continuous, poincare_inequality))
        self.add(cube)

        self.wait(1)

        one_text = TexMobject("1=")
        n_text = TexMobject("=n")

        one_text.next_to(poincare_inequality, LEFT)
        one_text.shift(0.05*UP)
        n_text.next_to(poincare_inequality, RIGHT)

        self.play(Write(one_text), Write(n_text))
        self.wait(5)
    
class KKLInequalityScene(Scene):
    def construct(self):

        ##################
        ## KKL statement
        ##################
        thm = TextMobject("Theorem [KKL 88]:")
        kkl1 = TexMobject(r"\mathrm{Var}[f]", r"\leq C")
        kkl2 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        kkl3 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", "\log(1/ \max_i \mathrm{Inf}_i(f))")

        kkl1.set_color_by_tex("\mathrm{Var}", RED_C)
        kkl2.set_color(BLUE_C)
        kkl3.set_color_by_tex(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
        kkl3.set_color_by_tex(r"\log", GREEN_C)
        
        thm.shift(2*UP + 3*LEFT)
        kkl1.next_to(thm, RIGHT)
        kkl2.next_to(kkl1, RIGHT)
        kkl3.next_to(kkl1, RIGHT)

        initial_statement = VGroup(kkl1,kkl2)
        initial_statement.next_to(thm, RIGHT)
        
        self.play(Write(thm))
        self.play(Write(initial_statement, run_time = 1))
        self.wait(1)
        self.play(Transform(kkl2, kkl3))
        self.wait(2)
        ###################
        ## Draw tribes function
        ###################
        
        length = 6
        voffset = 2

        tribes = VGroup()

        hline = Line(length*LEFT, length*RIGHT)
        tribes.add(hline)

        large_line_positions = np.linspace(-length, length,6)
        for x in large_line_positions:
            lvline = Line(x*RIGHT + UP, x*RIGHT)
            tribes.add(lvline)

        self.lvlines_for_digits = []
            
        small_line_positions = np.linspace(-length, -length + 4*length/5, 11)
        for x in small_line_positions:
            lvline = Line(x*RIGHT + 0.5* UP, x*RIGHT)
            self.lvlines_for_digits.append(lvline)
            tribes.add(lvline)

        self.lvlines_for_digits = self.lvlines_for_digits[:-1]
        small_line_positions = np.linspace(length - 4*length/5, length, 11)
        for x in small_line_positions:
            lvline = Line(x*RIGHT + 0.5* UP, x*RIGHT)
            self.lvlines_for_digits.append(lvline)
            tribes.add(lvline)

        elipsis_text = TexMobject(r"\cdots \cdots")
        elipsis_text.next_to(hline, UP)
        tribes.add(elipsis_text)

        tribes_text = TextMobject("Tribes:")
        tribes_text.next_to(elipsis_text, UP)
        tribes_text.shift(2*UP)
        tribes.add(tribes_text)

        tr1 = TexMobject(r"T_1")
        tr2 = TexMobject(r"T_2")
        tr3 = TexMobject(r"T_{k-1}")
        tr4 = TexMobject(r"T_k")

        tr1.next_to(elipsis_text, UP)
        tr1.shift(UP)
        tr1.shift(4.6*LEFT)

        tr2.next_to(elipsis_text, UP)
        tr2.shift(UP)
        tr2.shift(2.3*LEFT)
        
        tr3.next_to(elipsis_text, UP)
        tr3.shift(UP)
        tr3.shift(2.6*RIGHT)

        tr4.next_to(elipsis_text, UP)
        tr4.shift(UP)
        tr4.shift(4.9*RIGHT)

        tribes.add(tr1)
        tribes.add(tr2)
        tribes.add(tr3)
        tribes.add(tr4)
        
        tribes.shift(voffset*DOWN)
        self.play(ShowCreation(tribes))
        
        empty_text = TextMobject("-"*15)
        brace = Brace(empty_text)
        brace.next_to(hline, DOWN)
        brace.shift(0.8*length * LEFT)        
        tribe_size_text = TexMobject(r"\log n - \log \log n")
        tribe_size_text.next_to(brace, DOWN)

        self.wait(2)
        
        self.lvlines_for_digits = self.lvlines_for_digits[:-1]

        self.tribe_digits_to_draw = []

        random.seed(566)
        random_digits = [random.choice(["+", "-"]) for i in self.lvlines_for_digits]
        self.add_tribes_digits(random_digits)
        self.wait(1)
        
        random_digits = [random.choice(["+", "-"]) for i in self.lvlines_for_digits]
        self.transform_tribes_digits(random_digits)
        self.wait(1)
        
        random_digits = [random.choice(["+", "-"]) for i in self.lvlines_for_digits]
        random_digits[4] = "-"
        for i in range(5,10):
            random_digits[i] = "+"
        self.transform_tribes_digits(random_digits)

        self.wait(2)
        r = Rectangle()
        r.next_to(elipsis_text, UP)
        r.set_color(YELLOW)
        r.scale(0.7)
        r.shift(0.4 * length *  LEFT + DOWN)
        self.play(ShowCreation(r))

        self.wait(1)
        
        self.play(Write(brace))
        self.play(Write(tribe_size_text))
        
        implies_text = TexMobject(r"\longrightarrow")
        implies_text.next_to(tribe_size_text, RIGHT)

        tribes_influence_text = TexMobject(r"\mathrm{Inf}_i(\mathrm{Tribes})", r" = \mathcal{O} \left( \frac{\log n}{n} \right)")
        tribes_influence_text.set_color_by_tex("Tribes", BLUE_C)
        tribes_influence_text.next_to(implies_text, RIGHT)
        
        self.play(Write(implies_text))
        self.play(Write(tribes_influence_text))
        
        self.wait(2)

        self.play(*[FadeOut(x, LEFT) for x in ([tribes, r, implies_text, tribe_size_text, brace] + self.tribe_digits_to_draw)])
        self.play(tribes_influence_text.shift, 3*LEFT + 2*UP, tribes_influence_text.scale, 0.8)

        maj_influence_text = TexMobject(r"\mathrm{Inf}_i(\mathrm{Majority})", r" = \mathcal{O} \left(\frac{1}{\sqrt{n}}\right)")
        maj_influence_text.scale(0.8)
        maj_influence_text.set_color_by_tex("Majority", BLUE_C)
        maj_influence_text.next_to(tribes_influence_text, RIGHT)
        maj_influence_text.shift(RIGHT)

        p1 = 0.5*RIGHT*(maj_influence_text.get_x()+tribes_influence_text.get_x())
        p1 += UP * maj_influence_text.get_y() + UP
        p2 = p1 + 10*DOWN
        vline = Line(p1, p2)

        self.play(ShowCreation(vline))
        self.wait(1)
        
        ### Tribes calculations
        should_do_tribes_calculations = False
        if should_do_tribes_calculations:
            t1 = TexMobject(r"\mathrm{Var}[f]")
            t2 = TexMobject(r"\leq C")
            t3 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", "\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t4 = TexMobject(r"\sum_{i=1}^{n}\frac{\log n}{n}", r"\over", "\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t5 = TexMobject(r"\log n", r"\over", r"\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t6 = TexMobject(r"\log n", r"\over", r"\log \left( n / \log n \right)")
            one = TexMobject("1")

            for t in [t1,t2,t3,t4,t5,t6]:
                t.scale(0.8)

            t1.set_color(RED_C)
            t2.next_to(t1, RIGHT)
            t3.next_to(t2, RIGHT)
            t3.set_color_by_tex(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
            t3.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)

            v = VGroup(t1, t2, t3)
            v.next_to(tribes_influence_text, DOWN)
            v.shift(DOWN + 0.1*LEFT)

            one.next_to(t2, LEFT)

            t4.next_to(t2, RIGHT)
            t4.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)
            t5.next_to(t2, RIGHT)
            t5.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)
            t6.next_to(t2, RIGHT)

            self.play(Write(v))
            self.wait(1)
            self.play(Transform(t1, one))
            self.wait(1)
            self.play(Transform(t3, t4))
            self.wait(1)
            self.play(Transform(t3, t5))
            self.wait(1)
            self.play(Transform(t3, t6))
            self.wait(1)

            tight = TextMobject("KKL","tight")
            tight.set_color_by_tex("tight", GREEN_D)
            tight.next_to(tribes_influence_text, DOWN)
            tight.shift(DOWN)
            self.play(Transform(v, tight))
            self.wait(1)
        else:
            tight = TextMobject("KKL","tight")
            tight.set_color_by_tex("tight", GREEN_D)
            tight.next_to(tribes_influence_text, DOWN)
            tight.shift(DOWN)
            self.play(Write(tight))
            self.wait(1)
                
        ### Majority calculations

        self.play(Write(maj_influence_text))
        self.wait(1)
        
        should_do_majority_calculations = False
        if should_do_majority_calculations:        
            t1 = TexMobject(r"\mathrm{Var}[f]")
            t2 = TexMobject(r"\leq C")
            t3 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", "\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t4 = TexMobject(r"\sum_{i=1}^{n}\frac{1}{\sqrt{n}}", r"\over", "\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t5 = TexMobject(r"\sqrt{n}", r"\over", r"\log(\max_i 1/ \mathrm{Inf}_i(f))")
            t6 = TexMobject(r"\sqrt{n}", r"\over", r"1/2 \log \left(n \right)")
            one = TexMobject("1")

            for t in [t1,t2,t3,t4,t5,t6]:
                t.scale(0.8)

            t1.set_color(RED_C)
            t2.next_to(t1, RIGHT)
            t3.next_to(t2, RIGHT)
            t3.set_color_by_tex(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
            t3.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)

            v = VGroup(t1, t2, t3)
            v.next_to(maj_influence_text, DOWN)
            v.shift(DOWN + 0.1*RIGHT)

            one.next_to(t2, LEFT)

            t4.next_to(t2, RIGHT)
            t4.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)
            t5.next_to(t2, RIGHT)
            t5.set_color_by_tex(r"\log(\max_i 1/ \mathrm{Inf}_i(f))", GREEN_C)
            t6.next_to(t2, RIGHT)

            self.play(Write(v))
            self.play(Transform(t1, one))
            self.play(Transform(t3, t4))
            self.play(Transform(t3, t5))
            self.play(Transform(t3, t6))

            loose = TextMobject("KKL","not tight")
            loose.set_color_by_tex("not tight", RED_D)
            loose.next_to(maj_influence_text, DOWN)
            loose.shift(DOWN)        
            self.play(Transform(v, loose))
        else:
            loose = TextMobject("KKL","not tight")
            loose.set_color_by_tex("not tight", RED_D)
            loose.next_to(maj_influence_text, DOWN)
            loose.shift(DOWN)
            self.play(Write(loose))
                
        self.wait(5)
        

    def set_tribes_digits_to_draw(self, tribes_digits):
        self.tribe_digits_to_draw = []
        for l,d in zip(self.lvlines_for_digits, tribes_digits):
            t = TexMobject(d)
            t.set_color(YELLOW)
            t.scale(0.8)
            t.next_to(l,RIGHT)
            t.shift(0.13*LEFT)
            self.tribe_digits_to_draw.append(t)            

    def add_tribes_digits(self, tribe_digits):
        for d in self.tribe_digits_to_draw:
            self.remove(d)
        self.set_tribes_digits_to_draw(tribe_digits)
        self.add(*self.tribe_digits_to_draw)

    def transform_tribes_digits(self, tribe_digits):
        old_tribe_digits = self.tribe_digits_to_draw
        self.set_tribes_digits_to_draw(tribe_digits)
        self.play(*[ClockwiseTransform(old, new) for (old,new) in zip(old_tribe_digits, self.tribe_digits_to_draw)])
        self.tribe_digits_to_draw = old_tribe_digits        
            

class SensitivityScene(HypercubeScene):
    def construct(self):
        sensitivity = TexMobject(r"h_f(x) = \#\{i \mid f(x) \neq f(x^{\oplus i})\}")
        sensitivity.shift(2*UP + 2*LEFT)
        
        self.play(Write(sensitivity))
        # self.add(sensitivity)
        self.wait(1)
        
        h = Hypercube(shift=4*RIGHT + 0.2*DOWN)
        to_draw = h.get_sorted_drawing()
        random.seed(55)
        function_indicator = random.sample(h.nodes, 8)
        function_nodes = [h.nodes_to_draw[v] for v in function_indicator]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)
        self.play(*[GrowFromCenter(x) for x in to_draw])

        
        random.seed(656)
        number_of_random = 4
        random_nodes = random.sample(h.nodes, number_of_random)
        for source_node in random_nodes:
            target_nodes = [v for v in h.get_neighbors(source_node) if (v in function_indicator and source_node not in function_indicator) or (v not in function_indicator and source_node in function_indicator)]
            self.highlight_edges_from_source(h, source_node, target_nodes, speed_factor = 0.7)

        self.wait(1)
            
        for source_node in function_indicator:
            target_nodes = [v for v in h.get_neighbors(source_node) if (v in function_indicator and source_node not in function_indicator) or (v not in function_indicator and source_node in function_indicator)]
            self.highlight_edges_from_source(h, source_node, target_nodes, speed_factor = 0.4, should_return_to_original_edges=False)
            
        self.wait(1)
        
        inf_sen_eq = TexMobject("\mathbb{E}h_f", r" = ", r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        inf_sen_eq.set_color_by_tex("mathbb", YELLOW_C)
        inf_sen_eq.set_color_by_tex("Inf", BLUE_C)
        inf_sen_eq.align_to(sensitivity, LEFT)
        inf_sen_eq.shift(0.5*DOWN + 0.5 * RIGHT)
        # self.add(inf_sen_eq)
        self.play(Write(inf_sen_eq))

        self.wait(2)

        poincare_inequality = TexMobject(r"\text{Poincar\'e Inequality}:")
        p1 = TexMobject(r"\mathrm{Var}[f]", r"\leq")
        p2 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        p3 = TexMobject(r"\mathbb{E}h_f")

        p1.set_color_by_tex("Var", RED_C)
        p2.set_color(BLUE_C)
        p3.set_color(YELLOW_C)
        p1.next_to(poincare_inequality, RIGHT)
        p2.next_to(p1, RIGHT)
        v = VGroup(poincare_inequality, p1, p2)
        v.align_to(sensitivity, LEFT)
        v.shift(3*DOWN)
        p3.next_to((p1), RIGHT)

        # self.add(v)
        self.play(Write(v))
        self.wait(1)
        self.play(Transform(p2, p3))        
        
        self.wait(5)


class TalagrandInequalityScene(Scene):
    def construct(self):
        thm = TextMobject("Theorem [Talagrand 93]:")
        tal1 = TexMobject(r"\mathrm{Var}[f]", r"\leq C")
        tal2 = TexMobject(r"\mathbb{E}h_f")
        tal3 = TexMobject(r"\mathbb{E}\sqrt{h_f}")

        tal1.set_color_by_tex("\mathrm{Var}", RED_C)
        tal2.set_color(YELLOW_C)
        tal3.set_color(YELLOW_C)
        
        thm.shift(2*UP + 3*LEFT)
        tal1.next_to(thm, RIGHT)
        tal2.next_to(tal1, RIGHT)
        tal3.next_to(tal1, RIGHT)
        tal3.shift(0.05*UP) # Needed bc sqrt offsets the center a bit.

        initial_statement = VGroup(tal1,tal2)
        initial_statement.next_to(thm, RIGHT)
        
        self.play(Write(thm))
        self.play(Write(initial_statement, run_time = 1))
        self.wait(1)
        self.play(Transform(tal2, tal3))

        self.wait(2)
    
        tribes_sensitivity_text = TexMobject(r"\mathbb{E}\sqrt{h_{\mathrm{Tribes}}}", r" = \Omega \left(\sqrt{\log n} \right)")
        tribes_sensitivity_text.scale(0.8)
        tribes_sensitivity_text.set_color_by_tex("Tribes", YELLOW_C)
        tribes_sensitivity_text.shift(2.5*LEFT)

        self.play(Write(tribes_sensitivity_text))        

        maj_sensitivity_text = TexMobject(r"\mathbb{E}\sqrt{h_{\mathrm{Majority}}}", r" = \mathcal{O}(1)")
        maj_sensitivity_text.scale(0.8)
        maj_sensitivity_text.set_color_by_tex("Majority", YELLOW_C)
        maj_sensitivity_text.next_to(tribes_sensitivity_text, RIGHT)
        maj_sensitivity_text.shift(RIGHT)

        p1 = 0.5*RIGHT*(maj_sensitivity_text.get_x()+tribes_sensitivity_text.get_x())
        p1 += UP * maj_sensitivity_text.get_y() + UP
        p2 = p1 + 10*DOWN
        vline = Line(p1, p2)

        self.play(ShowCreation(vline))

        self.wait(1)

        loose = TextMobject("Talagrand","not tight")
        loose.set_color_by_tex("not tight", RED_D)
        loose.next_to(tribes_sensitivity_text, DOWN)
        loose.shift(DOWN)
        self.play(Write(loose))
        self.wait(1)

        self.play(Write(maj_sensitivity_text))    

        tight = TextMobject("Talagrand","tight")
        tight.set_color_by_tex("tight", GREEN_D)
        tight.next_to(maj_sensitivity_text, DOWN)
        tight.align_to(loose, DOWN)        
        self.play(Write(tight))
        
        self.wait(5)


class DiamondScene(Scene):
    def construct(self):

        ##############################
        ### Draw initial diamond
        ##############################
        
        poincare_text = TexMobject(r"\mathrm{Var}[f]", r"\leq", r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)")
        kkl_text =TexMobject(r"\mathrm{Var}[f]", r"\leq C", r"{\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", r"\log(1/ \max_i \mathrm{Inf}_i(f))}")
        tal_text = TexMobject(r"\mathrm{Var}[f]", r"\leq C", r"\mathbb{E} \sqrt{h_f}")

        poincare_text.set_color_by_tex("Var", RED_C)
        poincare_text.set_color_by_tex("Inf", BLUE_C)
        poincare_text.shift(2.9*UP)

        kkl_text.set_color_by_tex("Var", RED_C)
        kkl_text.set_color_by_tex("\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
        kkl_text.set_color_by_tex("log", GREEN_C)
        kkl_text.shift(4*LEFT)
        kkl_text.scale(0.8)

        kkl2 = TextMobject("KKL")
        kkl2.align_to(kkl_text, LEFT)
        kkl2.shift(UP)

        tal_text.set_color_by_tex("Var", RED_C)
        tal_text.set_color_by_tex("sqrt", YELLOW_C)
        tal_text.shift(4*RIGHT)

        tal2 = TextMobject("Talagrand")
        tal2.align_to(tal_text, RIGHT)
        tal2.shift(UP)

        l1 = Arrow()
        l1.put_start_and_end_on(kkl_text.get_edge_center(UP), poincare_text.get_edge_center(DOWN))
        l1.scale(0.6)
        l1.set_color(GRAY)

        l2 = Arrow()
        l2.put_start_and_end_on(tal_text.get_edge_center(UP), poincare_text.get_edge_center(DOWN))
        l2.scale(0.6)
        l2.set_color(GRAY)

        tal_conj = TexMobject(r"\mathrm{Var}[f]",
                              r"\sqrt{\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)}",
                              r"\leq C", r"\mathbb{E}\sqrt{h_f}")
        tal_conj.set_color_by_tex(r"\mathbb{E}\sqrt{h_f}", YELLOW_C)
        tal_conj.set_color_by_tex(r"Var", RED_C)
        tal_conj.set_color_by_tex(r"Inf", GREEN_C)
        tal_conj.shift(3.*DOWN)
        tal_conj.scale(0.9)

        l3 = Arrow()
        l3.put_start_and_end_on(tal_conj.get_edge_center(UP), kkl_text.get_edge_center(DOWN))
        l3.scale(0.6)
        l3.set_color(GRAY)

        l4 = Arrow()
        l4.put_start_and_end_on(tal_conj.get_edge_center(UP), tal_text.get_edge_center(DOWN))
        l4.scale(0.6)
        l4.set_color(GRAY)

        mystery = TextMobject("???")
        mystery.shift(tal_conj.get_center())
        
        conj_text = TextMobject("Conj:")
        conj_text.next_to(tal_conj, LEFT)
        conj_text.align_to(kkl2, LEFT)

        thm_text = TextMobject("EG20:")
        thm_text.next_to(tal_conj, LEFT)
        thm_text.align_to(kkl2, LEFT)
        thm_text.shift(0.05*UP)

        r = Rectangle()
        r.stretch(3.4, 0)
        r.stretch(0.9, 1)
        r.align_to(thm_text, LEFT)
        r.align_to(tal_conj, DOWN)
        r.shift(0.2*LEFT + 0.2*DOWN)
        r.set_color(GOLD)
        r.set_stroke(width=10)

        self.play(Write(poincare_text))
        self.wait(1)
        self.play(Write(kkl_text), Write(kkl2), ShowCreation(l1))
        self.wait(1)
        self.play(Write(tal_text), Write(tal2), ShowCreation(l2))
        self.wait(1)
        self.play(ShowCreation(l3), ShowCreation(l4))
        self.wait(0.2)
        self.play(Write(mystery))
        self.wait(1)
        self.play(Write(conj_text), ReplacementTransform(mystery, tal_conj))
        self.wait(6)

        tal_conj_copy = tal_conj.copy()

        ##############################
        ### Explain kkl derivation
        ##############################        
        black_screen = Rectangle(fill_opacity=1, fill_color=BLACK, stroke_color=BLACK)
        black_screen.scale(10)
        
        to_prove = TexMobject(r"\text{To prove: } C \leq ", r"{\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", r"\log(1/\delta)}")
        to_prove.set_color_by_tex("mathrm{Inf}_i(f)", BLUE_C)
        to_prove.set_color_by_tex("log", GREEN_C)

        delta = TexMobject(r"\delta :=", r"\max_i \mathrm{Inf}_i(f)")
        delta.set_color_by_tex("Inf", GREEN_C)
        
        to_prove.shift(3*UP + 3*LEFT)
        delta.next_to(to_prove, RIGHT)
        delta.shift(2*RIGHT)

        prover1 = TexMobject(r"\mathrm{Var}[f]")
        prover2 = TexMobject(r"\sqrt{\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)}",
                              r"\leq")
        prover3 = TexMobject("C")
        prover4 = TexMobject(r"\mathbb{E}\sqrt{h_f}")
        prover5 = TexMobject(r"\sqrt{\mathbb{E}h_f}")
        prover6 = TexMobject(r"\sqrt{\sum_i \mathrm{Inf}_i(f)}")
        prover7 = TexMobject(r"\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)",
                              r"\leq")
        prover8 = TexMobject(r"\sum_i \mathrm{Inf}_i(f)")
        prover9 = TexMobject(r"\log \left(\frac{1}{ \sum_i \mathrm{Inf}_i(f)^2} \right)",
                              r"\leq")
        prover10 = TexMobject(r"\log \left(\frac{1}{ \delta \sum_i \mathrm{Inf}_i(f)} \right)",
                                     r"\leq")
        
        prover1.set_color(RED_C)
        prover2.set_color_by_tex(r"sqrt", GREEN_C)
        prover4.set_color_by_tex(r"\sqrt", YELLOW_C)
        prover6.set_color(BLUE_C)
        prover7.set_color_by_tex(r"log", GREEN_C)
        prover8.set_color(BLUE_C)
        prover9.set_color_by_tex(r"log", GREEN_C)
        prover10.set_color_by_tex(r"log", GREEN_C)

        prover1.shift(5*LEFT)

        prover2.next_to(prover1, RIGHT)
        prover3.next_to(prover2, RIGHT)        
        prover4.next_to(prover3, RIGHT)        
        
        v = VGroup(prover1, prover2, prover3, prover4)
        v.next_to(to_prove, DOWN)
        v.shift(2.5*RIGHT + 0.5 * DOWN)

        prover5.next_to(prover2, RIGHT)
        prover6.next_to(prover2, RIGHT)

        # self.play(GrowFromCenter(black_screen, run_time=2), ReplacementTransform(tal_conj_copy, v))
        self.add(black_screen)
        self.play(ReplacementTransform(tal_conj_copy, v))
        self.wait(1)

        self.play(Write(to_prove), Write(delta))

        self.wait(2)
        self.play(FadeOut(prover1), prover3.next_to, prover2, LEFT, prover4.next_to, prover2, RIGHT)
        self.wait(0.5)
        
        prover7.next_to(prover3, RIGHT)
        prover8.next_to(prover7, RIGHT)
        prover8.shift(0.1*DOWN)
        prover9.next_to(prover8, LEFT)
        prover9.shift(0.1*UP)
        prover10.next_to(prover8, LEFT)
        prover10.shift(0.1*UP)
            
        self.play(ReplacementTransform(prover4, prover5))
        self.wait(0.5)    
        self.play(ReplacementTransform(prover5, prover6))
        self.wait(0.5)
        self.play(ReplacementTransform(prover6, prover8), ReplacementTransform(prover2, prover7))
        self.wait(0.5)
        self.play(ReplacementTransform(prover7, prover9), prover3.next_to, prover9, LEFT)
        self.wait(0.5)
        self.play(ReplacementTransform(prover9, prover10))
        self.wait(0.5)
        vline = Line(0.5*DOWN, 10*DOWN)
        self.add(vline)

        case1 = TexMobject(r"\delta \sum_i \mathrm{Inf}_i(f) \leq \delta^{1/2}")
        case2 = TexMobject(r"\delta \sum_i \mathrm{Inf}_i(f) \geq \delta^{1/2}")

        case1.set_color(GOLD_C)
        case2.set_color(GOLD_C)
        
        case1.shift(2.5*LEFT + 1*DOWN) 
        self.play(Write(case1))
        self.wait(0.5)
        
        s1 = TexMobject("C", r"\log(1/\delta^{1/2})", r"\leq", r"\sum_i \mathrm{Inf}_i(f)")
        s1.set_color_by_tex("Inf", BLUE_C)
        s1.set_color_by_tex("log", GREEN_C)
        s2 = TexMobject(r"\frac{1}{2} C", r"\log(1/\delta)", r"\leq", r"\sum_i \mathrm{Inf}_i(f)")
        s2.set_color_by_tex("Inf", BLUE_C)
        s2.set_color_by_tex("log", GREEN_C)

        s1.shift(3.2*LEFT, 3*DOWN)
        s1.align_to(case1, RIGHT)
        s2.shift(3*LEFT, 3*DOWN)
        s2.align_to(case1, RIGHT)

        v2 = VGroup(prover8, prover10)
        vcopy = v2.copy()
        
        self.play(ReplacementTransform(vcopy, s1))
        self.wait(0.5)
        self.play(ReplacementTransform(s1, s2))
        self.wait(0.5)
        
        case2.shift(2.5*RIGHT + 1*DOWN) 
        self.play(Write(case2))
        self.wait(0.5)
        
        c1 = TexMobject(r"\sum_i \mathrm{Inf}_i(f)", r"\geq ")
        c2 = TexMobject(r"\frac{1}{\delta^{1/2}}")
        c3 = TexMobject(r"\log(1/\delta)")

        c1.set_color_by_tex("Inf", BLUE_C)
        c2.set_color(GREEN_C)
        c3.set_color(GREEN_C)
        
        c1.shift(2.5*RIGHT, 3*DOWN)
        c1.align_to(case2, LEFT)
        c1.align_to(s2, DOWN)
        c2.next_to(c1, RIGHT)
        c2.shift(0.2*UP)
        c3.next_to(c1, RIGHT)
        c3.shift(0.2*UP)

        self.play(Write(c1), Write(c2))
        self.wait(0.5)
        self.play(ReplacementTransform(c2, c3))

        
        
        self.wait(5)

        to_remove = [vline, to_prove, delta, c1,c2,c3, s1,s2, case1, case2, prover1, prover2, prover3, prover4, prover5, prover6, prover7, prover8, prover9, prover10]
        self.remove(*to_remove)        
        self.play(FadeOut(black_screen))

        self.wait(5)

        ############################
        ### Back to diamond
        ############################

        self.remove(tal_conj)
        self.play(ClockwiseTransform(conj_text, thm_text), Write(tal_conj), ShowCreation(r), run_time=2)
        
        self.wait(5)

        
class StrengtheningScene(Scene):

    def construct(self):

        thm = TextMobject("Thm [EG20]: ")
        t1 = TexMobject(r"\mathrm{Var}[f]")
        t2 = TexMobject(r"\sqrt{\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)}")
        t3 = TexMobject(r"\leq C")
        t4 = TexMobject(r"\mathbb{E}\sqrt{h_f}")

        t5 = TexMobject(r"\left(\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)\right)", r"^{p}")
        t6 = TexMobject(r"{\mathbb{E}h}", r"^{p}", r"_{f}")
        t7 = TexMobject(r"\vartriangleright \text{For all } 1/2 \leq p < 1")
       
        t8 = TexMobject(r"\exists g_f : \{-1,1\}^n \to [0,1] \text{ s.t.}")
        t9 = TexMobject(r"\vartriangleright \mathbb{E}g_f^2 \leq 2 \mathrm{Var}[f]")

        t10 = TexMobject(r"{\mathbb{E}[h}", r"^{p}", r"_{f}", r"g_f", r"]")

        
        t1.set_color(RED_C)
        t2.set_color(GREEN_C)
        t4.set_color(YELLOW_C)

        t5.set_color(GREEN_C)
        t5.set_color_by_tex("^{p}", PINK)
        t6.set_color(YELLOW_C)
        t6.set_color_by_tex("^{p}", PINK)

        t7.set_color(PINK)
        t9.set_color(ORANGE)
        t10.set_color(YELLOW_C)
        t10.set_color_by_tex("^{p}", PINK)
        t10.set_color_by_tex("g_f", ORANGE)

        thm.shift(2*UP + 5*LEFT)

        t1.shift(4.5*LEFT + 1.5 * DOWN)        
        t2.next_to(t1, RIGHT)
        t3.next_to(t2, RIGHT)
        t4.next_to(t3, RIGHT)

        t5.next_to(t1, RIGHT)
        t6.next_to(t3, RIGHT)
        t6.shift(0.05*DOWN)

        t8.next_to(thm, RIGHT)
        t9.next_to(t8, DOWN)
        t9.align_to(t8, LEFT)
        t9.shift(0.5*RIGHT)
        t7.next_to(t9, DOWN)
        t7.align_to(t9, LEFT)
        t10.next_to(t3, RIGHT)

        v = VGroup(t1, t2, t3, t4)

        self.add(thm)
        self.add(v)
        
        self.wait(2)
        self.play(ReplacementTransform(t2, t5), ReplacementTransform(t4, t6), Write(t7))
        self.wait(1)
        
        self.play(Write(t8), Write(t9))
        self.wait(1)
        self.play(ReplacementTransform(t6, t10))
        self.wait(1)
        r = Rectangle()
        r.stretch(2.7, 0)
        r.stretch(0.9, 1)
        r.align_to(v, LEFT)
        r.align_to(v, DOWN)
        r.shift(0.4*LEFT + 0.2*DOWN)
        r.set_color(GOLD)
        r.set_stroke(width=10)

        self.play(ShowCreation(r))
        
        self.wait(5)

class AnotherTalagrandInequalityScene(HypercubeScene):

    def construct(self):

        ##########################
        ### Talagrand inequality statement
        ##########################
        
        thm = TextMobject("Theorem [Talagrand 94]: ")
        t1 = TexMobject(r"\mathrm{Var}[f]", r"\leq C")
        t2 = TexMobject(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", r"\over", "\log(1/ \max_i \mathrm{Inf}_i(f))")
        t3 = TexMobject(r"\sum_{i=1}^{n}", r"{\mathrm{Inf}_i(f)", r"\over", "1 + \log(1/ \mathrm{Inf}_i(f))}")
        
        thm.shift(3*UP + 3.5*LEFT)
        t1.next_to(thm, RIGHT)
        t2.next_to(t1, RIGHT)
        t3.next_to(t1, RIGHT)

        t1.set_color_by_tex("Var", RED_C)
        t2.set_color_by_tex(r"\sum_{i=1}^{n}\mathrm{Inf}_i(f)", BLUE_C)
        t2.set_color_by_tex(r"\log(1/ \max_i \mathrm{Inf}_i(f))", GREEN_C)
        t3.set_color(BLUE_C)
        t3.set_color_by_tex(r"1 + \log(1/ \mathrm{Inf}_i(f))", GREEN_C)
        
        v = VGroup(t1, t2)
                   
        self.play(Write(thm))
        self.wait(1)
        self.play(Write(v))
        self.wait(1)
        self.play(ReplacementTransform(t2, t3))
        self.wait(1)

        ##########################
        ### Boundaries
        ##########################
        
        s1 = TexMobject("\partial^+ f = \{x \in \{-1,1\}^n \mid f(x) = 1,~ h_f(x) > 0\}")
        s1.scale(0.8)
        s1.align_to(thm, LEFT)
        s1.set_color(BLUE_C)
        s1.shift(0.7*UP)

        s2 = TexMobject("\partial^- f = \{x \in \{-1,1\}^n \mid f(x) = -1,~ h_f(x) > 0\}")
        s2.scale(0.8)
        s2.next_to(s1, DOWN)        
        s2.align_to(thm, LEFT)
        s2.shift(0.2*DOWN)
        s2.set_color(RED_C)

        h = Hypercube(shift=4.5*RIGHT + 0.4*UP, scale=0.9)

        function_indicator = [(1,-1,-1), (-1,-1,-1), (1,1,-1), (1,-1,1), (-1,1,-1), (1,1,1), (0.5,-0.5,-0.5)]
        non_function_indicator = [v for v in h.nodes if v not in function_indicator]
        function_nodes = [h.nodes_to_draw[v] for v in function_indicator]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

        inner_vertex_boundary = [v for v in function_indicator if len(set(h.get_neighbors(v)).intersection(non_function_indicator)) > 0]
        outer_vertex_boundary = [v for v in non_function_indicator if len(set(h.get_neighbors(v)).intersection(function_indicator)) > 0]

        self.play(Write(s1))
        self.wait(1)
        self.play(*[GrowFromCenter(x) for x in h.get_sorted_drawing()])
        self.wait(3)        
        
        for v in inner_vertex_boundary:
            node_to_draw = h.nodes_to_draw[v]
            self.play(node_to_draw.set_stroke, BLUE_C, 8, run_time=0.4)

        self.wait(2)
        self.play(Write(s2))
        self.wait(1)
        
        for v in outer_vertex_boundary:
            node_to_draw = h.nodes_to_draw[v]
            self.play(node_to_draw.set_stroke, RED_C, 8, run_time=0.4)
        
        self.wait(1)

        ##########################
        ### Our theorem
        ##########################
        
        our_thm1 = TextMobject("Theorem [EG20]: ")
        our_thm2 = TextMobject("If")
        t1 = TexMobject(r"\mathrm{Var}[f]", r"\geq C'", r"\sum_{i=1}^{n}", r"{\mathrm{Inf}_i(f)", r"\over", "1 + \log(1/ \mathrm{Inf}_i(f))}", r",")
        t2 = TexMobject(r"\text{then }", r"\mu(", r"\partial^+f", r")", r"\geq C", r"\mathrm{Var}[f]", ".")
        
        t1.set_color_by_tex("Var", RED_C)
        t1.set_color_by_tex("Inf", BLUE_C)
        t1.set_color_by_tex(r"1 + \log(1/ \mathrm{Inf}_i(f))", GREEN_C)
        t2.set_color_by_tex("partial", BLUE_C)
        t2.set_color_by_tex("Var", RED_C)


        our_thm1.align_to(thm, LEFT)
        our_thm1.shift(2.4*DOWN)
        our_thm2.next_to(our_thm1, RIGHT)
        t1.next_to(our_thm2, RIGHT)
        t1.shift(0.1*DOWN)
        t2.align_to(our_thm1, LEFT)
        t2.shift(3.5*DOWN)

        self.play(Write(our_thm1))
        self.wait(1)
        self.play(Write(our_thm2), Write(t1))
        self.wait(2)
        self.play(Write(t2))
        self.wait(1)
        
        r = Rectangle()
        r.stretch(3.2, 0)
        r.stretch(1.2, 1)
        r.align_to(t2, LEFT)
        r.align_to(t2, DOWN)
        r.shift(0.3*LEFT + 0.2*DOWN)
        r.set_color(GOLD)
        r.set_stroke(width=10)
        
        self.play(ShowCreation(r))
        
        self.wait(5)


class PathwiseIntroScene(GraphScene, HypercubeScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 2**4,
        "y_min" : -1.1,
        "y_max" : 1.1,
        "x_axis_width" : 6.8,
        "y_axis_height": 3.7,
        "graph_origin" : 2*DOWN ,
        "function_color" : YELLOW ,
        "axes_color" : GREEN,
        "x_labeled_nums" :range(2**4+1),
        "y_tick_frequency": 0.2,
    }

    def construct(self):

        ###################################
        ## Expectations and derivatives
        ###################################
        
        t1 = TextMobject("Everything is expectation:")
        t2 = TexMobject(r"\vartriangleright", r"\mathbb{E}\sqrt{h_f}")
        t3 = TexMobject(r"\vartriangleright", r"\mathrm{Var}[f]", r"= \mathbb{E}(f - \mathbb{E}f)^2")
        t4 = TexMobject(r"\vartriangleright", r"\sum_i \mathrm{Inf}_i(f)", r"=",  r"~???" )
        t5 = TexMobject(r"\mathbb{E}||\nabla f ||^2")
        t6 = TexMobject(r"\partial_i f(x) = \frac{f(x^{i \to 1}) - f(x^{i\to -1})}{2}", "~")
        t7 = TexMobject(r" = \pm 1")
        t9 = TexMobject(" = 0")
        t10 = TexMobject(r"\partial_i f(x)^2 \in \{0,1\}")
        t11 = TexMobject(r"\mathbb{E}(\partial_i f)^2 = \mathrm{Inf}_i(f)")

        t2.set_color_by_tex("sqrt", YELLOW_C)
        t3.set_color_by_tex("Var", RED_C)
        t4.set_color_by_tex("Inf", BLUE_C)

        t1.to_corner(LEFT+UP)
        t2.next_to(t1, DOWN)
        t2.align_to(t1, LEFT)
        t2.shift(1*RIGHT)
        t3.next_to(t2, DOWN)
        t3.align_to(t2, LEFT)
        t4.next_to(t3, DOWN)
        t4.align_to(t3, LEFT)
        t5.next_to(t4[-2], RIGHT)
        t6.align_to(t1, LEFT)
        t6.shift(1*DOWN)
        t7.next_to(t6, RIGHT)
        t9.next_to(t6, RIGHT)
        t10.next_to(t6, RIGHT)
        t10.to_edge(RIGHT)
        t10.shift(1.3*LEFT)
        t11.next_to(t10, 0)

        self.wait(2)
        self.play(Write(t1))
        self.wait(2)
        self.play(Write(t2))
        self.wait(2)
        self.play(Write(t3))
        self.wait(2)
        self.play(Write(t4))
        self.wait(2)
        
        self.play(Write(t6))
        self.wait(2)

        h = Hypercube(shift=3.5*RIGHT + 1.7*UP, scale=0.9)
        function_indicator = [(-1,1,-1), (1,-1,-1), (1,-1,1), (1,1,1)]
        function_nodes = [h.nodes_to_draw[v] for v in function_indicator]
        for node_to_draw in function_nodes:
            node_to_draw.set_fill(color=WHITE)

        self.play(*[GrowFromCenter(x) for x in h.get_sorted_drawing()])
        self.wait(2)

        self.play(Transform(t6[-1], t7))
        self.highlight_edge(h, (-1,-1,-1), (-1,1,-1))
        self.wait(2)
        # self.play(Transform(t6[-1], t8))
        self.highlight_edge(h, (1,-1,-1), (1,1,-1))
        self.wait(2)
        self.play(Transform(t6[-1], t9))
        self.highlight_edge(h, (1,-1,1), (1,1,1))
        self.wait(2)

        self.play(Write(t10))
        self.wait(2)
        self.play(ReplacementTransform(t10, t11))
        self.wait(2)
        
        self.play(Transform(t4[-1], t5), Uncreate(t6), Uncreate(t11))
        self.wait(2)

        #####################################
        ## Revealing bits all at once
        #####################################

        length = 3
        k = 4
        n = 2**k
        uniform_var = VGroup()

        uniform_var = BinaryInput(n, length=length)

        uniform_var.group.next_to(t1, 0)
        uniform_var.group.shift(5.3*DOWN + 0.2*RIGHT)
        
        random.seed(566)
        random_digits = [random.choice(["+", "-"]) for i in range(n)]
        random_input = [1 if x == "+" else -1 for x in random_digits]
        uniform_var.set_digits_to_draw(random_digits)

        digits_to_draw = uniform_var.digits_to_draw       
        f = get_iterated_function(nand, k, 2)
        ftext = TexMobject("f(x) =")
        ftext.next_to(uniform_var.group, RIGHT)
        ftext.shift(3*RIGHT)
        fvalue = TextMobject("%d" %(f(random_input)))
        fvalue.next_to(ftext, RIGHT)

        self.play(ShowCreation(uniform_var.group))
        self.play(*[ShowCreation(d) for d in digits_to_draw])
        self.play(Write(ftext), Write(fvalue))

        self.wait(2)
        
        num_of_times_to_randomize = 1
        for i in range(num_of_times_to_randomize):
            old_to_draw = uniform_var.digits_to_draw[:]
            old_fvalue = fvalue
            
            new_random_digits = [random.choice(["+", "-"]) for i in range(n)]
            uniform_var.set_digits_to_draw(new_random_digits)
            digits_to_draw = uniform_var.digits_to_draw

            random_input = [1 if x == "+" else -1 for x in random_digits]
            fvalue = TexMobject("%d" %(-f(random_input),))
            fvalue.next_to(ftext, RIGHT)
            

            self.play(*[ReplacementTransform(old,new) for (old,new) in zip(old_to_draw, digits_to_draw)], ReplacementTransform(old_fvalue, fvalue))
            self.wait(2)
        
        self.play(FadeOut(fvalue), FadeOut(ftext), *[FadeOut(d) for d in digits_to_draw])

        #####################################
        ## Revealing bits one by one
        #####################################
        
        random.seed(25)
        initial_digits = ["?"] * n
        final_digits = [random.choice(["+", "-"]) for i in range(n)]
        final_bits = [1 if x == "+" else -1 for x in final_digits]

        xt = TexMobject(r"X_t", color=YELLOW)
        xt.next_to(uniform_var.group, 0)
        xt.shift(UP)
        
        uniform_var.set_digits_to_draw(initial_digits)
        self.play(*[ShowCreation(d) for d in uniform_var.digits_to_draw])
        self.play(Write(xt))
        self.wait(2)
        
        f = get_iterated_function(nand, k, 2)
        values = []
        values.append(calculate_expectation(f,n))
        for i,bit in enumerate(final_bits):
            f = fix_bit(f, i, bit)
            values.append(calculate_expectation(f,n))
        expected_f = interpolate_path(values, 0, n)
        
        self.setup_axes(animate=True)
        self.wait(2)        
        
        for i,new_digit in enumerate(final_digits):
            old_digit_to_draw = uniform_var.digits_to_draw[i]
            uniform_var.set_digit_to_draw(i, new_digit)
            if new_digit == "+":
                digit_animation = ClockwiseTransform(old_digit_to_draw, uniform_var.digits_to_draw[i])
            else:
                digit_animation = CounterclockwiseTransform(old_digit_to_draw, uniform_var.digits_to_draw[i])
            starting_time = i / n
            ending_time = (i+1)/n
            ## The ShowSegment actually changes the alpha, so we can't use it more than once per object.
            ## This means we have to recreate the function every time. Not ideal, but it works.
            func_graph = self.get_graph(expected_f, self.function_color)
            function_animation = ShowSegmentOfCreation(func_graph, starting_time, ending_time)
            self.play(digit_animation, function_animation, run_time = 0.3, rate_func=linear)

        func_graph = self.get_graph(expected_f, self.function_color)
        graph_label = self.get_graph_label(func_graph, label = r"\mathbb{E}f")
        graph_label.shift(0.2*LEFT)
        self.play(ShowCreation(graph_label))
        self.wait(5)
    

class MartingaleScene(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : -1,
        "y_max" : 1,
        "x_axis_width" : FRAME_WIDTH / 2,
        "y_axis_height": FRAME_HEIGHT / 2,
        "graph_origin" : 0.5*UP + FRAME_WIDTH/4 * LEFT,
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "x_axis_label": "$t$",
    }
    def construct(self):        
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)
        
        ##################################
        ### Introduce process
        ##################################
        
        n = 5
        uniform_var = BinaryInput(n, length=4)
        uniform_var.group.to_edge(UP)

        initial_digits = ["?"] * n
        uniform_var.set_digits_to_draw(initial_digits)

        self.add(uniform_var.group)
        self.add(*uniform_var.digits_to_draw)

        voffset = 1*DOWN
        
        hline = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        hline.shift(voffset)
        self.add(hline)

        vlines = []
        graphs = []
        arrows = []

        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]
        
        np.random.seed(420) # 167 is also not too bad, especially the first one (consider soloing it)
        for i,digit_to_draw in enumerate(uniform_var.digits_to_draw):
            b = get_b_path()            
            g = FunctionGraph(b, x_min=0, x_max=1, step_size=0.005, color=colors[i])
            g.make_jagged()
            g.scale_about_point(2, g.get_point_from_function(0))
            g.stretch_about_point(1.2, 0, g.get_point_from_function(0))
            g.to_edge(LEFT)
            g.shift(RIGHT*FRAME_WIDTH * i /n)
            g.shift(voffset)
            
            vline = Line(2*UP, 2*DOWN, color=DARK_GRAY)
            vline.to_edge(LEFT)
            vline.shift(RIGHT*FRAME_WIDTH * i /n)
            vline.shift(voffset)

            arrow = Arrow()
            arrow.put_start_and_end_on(voffset + 2*UP + LEFT_SIDE + RIGHT*FRAME_WIDTH * (i+0.5) /n, digit_to_draw.get_edge_center(DOWN))
            arrow.scale(0.6)
            arrows.append(arrow)
            
            self.add(vline)
            vlines.append(vline)

            graphs.append(g)

        self.wait(3)
        self.play(*[ShowCreation(g) for g in graphs], run_time=4, rate_func=linear)
        self.wait(2)
        for i, arrow, g in zip(range(n), arrows, graphs):
            new_digit = "+" if np.sign(g.get_function()(1)) == 1 else "-"
            old_digit_to_draw = uniform_var.digits_to_draw[i]
            uniform_var.set_digit_to_draw(i, new_digit)
            uniform_var.digits_to_draw[i].set_color(colors[i])
            self.play(ShowCreation(arrow), ReplacementTransform(old_digit_to_draw, uniform_var.digits_to_draw[i]))        
        # self.add(*graphs)
        # self.add(*arrows)

        self.wait(2)        
        # ##################################
        # ### Show martingaleness
        # ##################################

        self.play(*[FadeOut(arrow) for arrow in arrows],
                  *[FadeOut(vline) for vline in vlines],
                  *[FadeOut(digit) for digit in uniform_var.digits_to_draw],
                  FadeOut(uniform_var.group), FadeOut(hline))
                          
        # self.remove(*arrows)
        # self.remove(*vlines)
        # self.remove(*uniform_var.digits_to_draw)
        # self.remove(uniform_var.group)
        # self.remove(hline)

        self.setup_axes(animate=False)        
        self.remove(self.x_axis, self.y_axis)
        self.play(*[ReplacementTransform(g, self.y_axis) for g in graphs[1::2]],
                  *[ReplacementTransform(g, self.x_axis) for g in graphs[::2]])
        self.remove(*graphs)
        self.setup_axes(animate=False)
        
        t1 = TexMobject(r"X_t")
        t1.next_to(self.y_axis, LEFT)
        self.play(Write(t1))
        self.wait(2)
        
        np.random.seed(420)
        number_of_functions_to_show = 4
        for i in range(number_of_functions_to_show):
            b = get_b_path()
            g = self.get_graph(b, step_size=0.005, color=colors[i % len(colors)])
            g.make_jagged()
            self.play(ShowCreation(g), run_time=4, rate_func=linear)
            self.wait(1)
            self.remove(g)
        
        t2 = TexMobject(r"\text{Martingale: }", r"\mathbb{E}[X_t \mid X_s] = X_s")
        t2.next_to(self.axes, DOWN)

        t3 = TexMobject(r"B_t = \left( X_t^{(1)}, X_t^{(2)}, \ldots, X_t^{(n)} \right)")
        t3.next_to(t2, DOWN)
        self.play(Write(t2))
        self.wait(2)

        # #self.add(t2)
        # #self.add(t3) 
        
        number_of_functions_to_show = 8
        for i in range(number_of_functions_to_show):
            b = get_b_path()
            g = self.get_graph(b, step_size=0.005, color=colors[i % len(colors)])
            g.make_jagged()
            self.play(ShowCreation(g), run_time=4, rate_func=linear)
            self.wait(1) 
            self.remove(g)

        self.play(Write(t3))
        self.wait(2)

        ##################
        ## Show B(t)
        #################

        black_screen = Rectangle(fill_opacity=1, fill_color=BLACK, stroke_color=BLACK)
        black_screen.stretch(FRAME_WIDTH,0)
        black_screen.stretch(3,1)
        black_screen.shift(1.38*UP)
        self.add(black_screen)
        
        n = 3
        
        h = Hypercube(scale=1.5, shift=UP)
        m = h.transformation_matrix
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK, GRAY, WHITE]

        np.random.seed(434) # other candidates: 425, 431, 432
        functions  = []
        number_of_functions_to_show = 7
        for i in range(number_of_functions_to_show):

            B = get_B_path(n)
            b = get_b_path() # Shit! I forgot this here and based my seed choice with it. It must remain...

            if i == 5: ## Don't want the 6th one here, not very pretty
                continue

            f = lambda t: np.dot(m, B(t)[0])
            g = ParametricFunction(f, x_min=0, x_max=1, step_size=0.005, color=colors[i])
            g.shift(UP)
            g.set_stroke(width=7)
            g.make_jagged()
            functions.append(g)

        # Draw just the outer cube
        outer_cube = [np.array(x) for x in itertools.product([-1,1], repeat=3)]
        outer_cube = [tuple(v) for v in outer_cube]              
        outer_edges = [(v1,v2) for v1,v2 in itertools.combinations(outer_cube,2) if hamming_distance(v1,v2) == 1]
        to_draw = [h.edges_to_draw[v1,v2] for v1,v2 in outer_edges]
        for d in to_draw:
            d.set_stroke(color=GRAY)
            
        self.play(*[GrowFromCenter(d) for d in to_draw])
        for g in functions:
            self.play(ShowCreation(g), run_time=1.3, rate_func=linear)


        self.wait(2)
        self.play(*[FadeOut(d) for d in to_draw], *[FadeOut(g) for g in functions], FadeOut(black_screen))
        
        ##################################
        ### Show how to create using Brownian motion
        ##################################
        t4 = TextMobject("New maximums")
        t4.scale(1.2)
        t4.next_to(t3, 0)

        self.play(Transform(t3, t4), Transform(t2, t4))
        
        np.random.seed(167)
        number_of_functions_to_show = 3
        for i in range(number_of_functions_to_show):
            b, path, sign_change, increased_abs_value = get_b_path_complete()
            path = np.array([x[0] for x in path])
            increased_abs_value = np.array([x[0] for x in increased_abs_value])

            only_positive_increase_path = np.array([x if x in increased_abs_value and x > 0 else 0 for x in path])
            for i in range(1,len(only_positive_increase_path)):
                if only_positive_increase_path[i] == 0:
                    only_positive_increase_path[i] = only_positive_increase_path[i-1]

            only_negative_increase_path = np.array([x if x in increased_abs_value and x < 0 else 0 for x in path])
            for i in range(1,len(only_negative_increase_path)):
                if only_negative_increase_path[i] == 0:
                    only_negative_increase_path[i] = only_negative_increase_path[i-1]

            brownian_motion = interpolate_path(path, 0, 1)
            g_brownian = self.get_graph(brownian_motion, step_size=0.001, color=YELLOW)
            g_brownian.make_jagged()

            fp = interpolate_path(only_positive_increase_path, 0, 1)
            g_positive = self.get_graph(fp, step_size=0.001, color=GREEN)
            g_positive.make_jagged()

            fn = interpolate_path(only_negative_increase_path, 0, 1)
            g_negative = self.get_graph(fn, step_size=0.001, color=RED)
            g_negative.make_jagged()

            g_martingale = self.get_graph(b, step_size=0.001, color=YELLOW)
            g_martingale.make_jagged()

            self.play(ShowCreation(g_brownian), ShowCreation(g_positive), ShowCreation(g_negative), run_time=4, rate_func=linear)

            self.wait(2)
            self.play(ReplacementTransform(g_brownian, g_martingale), ReplacementTransform(g_positive, g_martingale), ReplacementTransform(g_negative, g_martingale))
            self.wait(2)
            self.remove(g_martingale)
        self.wait(5)


class HowToUseScene(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : -1,
        "y_max" : 1,
        "x_axis_width" : FRAME_WIDTH / 2,
        "y_axis_height": FRAME_HEIGHT / 2,
        "graph_origin" : FRAME_WIDTH/4 * LEFT,
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "y_axis_label": "$f_t = f(B_t)$",
        "x_axis_label": "$t$",
    }
        
    def construct(self):        

        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)
        
        t1 = TexMobject(r"f(x)", r"= \sum_{S \subseteq [n]}~~ \widehat{f}(S) \prod_{i\in S}", r"x_i")
        t11 = TexMobject(r"f(B_t)")
        t12 = TexMobject(r"B_t^{(i)}")

        t1.shift(3*UP)
        t11.next_to(t1[0], 0)
        t11.shift(0.1*LEFT)
        t12.next_to(t1[2], 0)
        t12.shift(0.1*RIGHT + 0.1*UP)
        
        self.wait(3)
        self.play(Write(t1))
        self.wait(2)
        self.play(Transform(t1[0], t11), Transform(t1[-1], t12))
        self.wait(2)

        self.setup_axes(animate=False)
        self.y_axis_label_mob.next_to(self.y_axis, LEFT)
        self.y_axis_label_mob.shift(0.3*DOWN)
        self.play(Write(VGroup(self.x_axis, self.y_axis)))

        np.random.seed(123)
        n = 9
        f = get_harmonic_extension(majority, n)
        
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]        
        number_of_functions_to_show = 5
        for i in range(number_of_functions_to_show):
            B = get_B_path(n)
            f_t = lambda t: f(B(t))
            g = self.get_graph(f_t, step_size=0.001, color=colors[i % len(colors)])
            g.make_jagged()
            self.play(ShowCreation(g), run_time=2, rate_func=linear)
            self.wait(1)

        jump_text = TexMobject(r"\text{Jump at $t$: } \Delta f(B_t) = 2t \partial_i f(B_t)")
        jump_text.to_edge(BOTTOM)
        jump_text.shift(DOWN)
        self.play(Write(jump_text))
            
        self.wait(10)


class QuadraticVariationScene(GraphScene):
    ## I want to use two graphs in the same scene, but unfortuantely, as far as I know,
    ## GraphScene only supports one set of axes. So I have to manually switch between
    ## them, if I want to use the automatic coordinate and axis ticks. It's not pretty,
    ## but it works.
    ## TODO: Extract the graphing functionality from GraphScene so that you can put in
    ## as many graphs as you want, wherever you want.
    
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : -1,
        "y_max" : 1,
        "x_axis_width" : FRAME_WIDTH / 2 - 1,
        "y_axis_height": FRAME_HEIGHT / 2,
        "graph_origin" : (FRAME_WIDTH/2-0.5) * LEFT + 0.5*UP,
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "y_axis_label": "",
        "x_axis_label": "$t$",
    }
    
    def construct(self):
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)

        t1 = TexMobject(r"f_t")
        t2 = TexMobject(r"\text{Quaratic variation: } [f]_t")        

        t1.shift(FRAME_WIDTH/4*LEFT + 3.5*UP)
        t2.shift(FRAME_WIDTH/4*RIGHT + 3.5*UP)
        
        self.setup_axes(animate=False)
        self.remove(self.x_axis, self.y_axis)
        x_axis1 = self.x_axis
        y_axis1 = self.y_axis
        x_axis2 = self.x_axis.copy()
        y_axis2 = self.y_axis.copy()              
        x_axis2.shift(FRAME_WIDTH/2 * RIGHT)
        y_axis2.shift(FRAME_WIDTH/2 * RIGHT)
        self.play(Write(t1), Write(t2), Write(VGroup(x_axis1, y_axis1)), Write(VGroup(x_axis2, y_axis2)))
        self.wait(2)
        
        t3 = TexMobject(r"\vartriangleright [f]_t = \sum_{s\leq t\text{ jumps}} \left( \Delta f_s \right)^2")
        t4 = TexMobject(r"\vartriangleright", r"\mathrm{Var}[f_t]", r"= \mathbb{E}[f]_t")

        t3.shift(FRAME_WIDTH/4 * LEFT + 2.5*DOWN)
        t4.next_to(t3, DOWN)
        t4.align_to(t3, LEFT)

        t4.set_color_by_tex("Var", RED_C)
        self.play(Write(t3), Write(t4))
        

        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]
        n = 3
        f = get_harmonic_extension(majority, n)

        np.random.seed(123)

        number_of_functions_to_show = 5
        for i in range(number_of_functions_to_show):
            B = get_B_path(n)            
            f_t = lambda t: f(B(t))
            
            self.x_axis = x_axis1
            self.y_axis = y_axis1
            gf = self.get_graph(f_t, step_size=0.001)
            gf.make_jagged()

            self.x_axis = x_axis2
            self.y_axis = y_axis2
            q = get_quadratic_variation(f_t, B, n)
            gq = self.get_graph(q, step_size=0.001)
            gq.make_jagged()

            self.play(ShowCreation(gf), ShowCreation(gq), run_time=3, rate_func=linear)
            self.wait(2)
            if i < number_of_functions_to_show-1:
                self.remove(gf, gq)
            
        t5 = TexMobject(r"\mathrm{Var}[f] = ", r"\mathrm{Var}[f(B_1)]")
        t6 = TexMobject(r"\mathbb{E} \sum_{s \leq 1\text{ jumps}} \left( \Delta f_s \right)^2")
        t7 = TexMobject(r"2 \mathbb{E} \sum_{i=1}^n \int_0^1 t (\partial_i f_t)^2 dt")

        t5.shift(2.5*RIGHT + 2.9*DOWN)
        t6.next_to(t5[0], RIGHT)
        t6.shift(0.2*DOWN)
        t7.next_to(t5[0], RIGHT)
        
        self.play(Write(t5))
        self.wait(2)
        self.play(Transform(t5[1], t6))
        self.wait(2)
        self.play(Transform(t5[1], t7))        

        r = Rectangle()
        r.stretch(1.7, 0)        
        r.next_to(t7, 0)
        r.shift(1*LEFT+0.1*UP)       
        r.set_color(GOLD)
        r.set_stroke(width=10)

        self.play(ShowCreation(r))

        # self.x_axis = x_axis2
        # self.y_axis = y_axis2

        # number_of_functions_to_show = 10
        # quadratic_variations = []
        # quadratic_graphs = []
        # for i in range(number_of_functions_to_show):
        #     print(i)
        #     B = get_B_path(n)            
        #     f_t = lambda t: f(B(t))
        #     q = get_quadratic_variation(f_t, B, n)
        #     gq = self.get_graph(q, step_size=0.001)
        #     gq.make_jagged()
        #     quadratic_variations.append(q)
        #     quadratic_graphs.append(gq)
        #     self.add(gq)

        # averaged_function = lambda t: np.mean([q(t) for q in quadratic_variations])

        # average_q = self.get_graph(averaged_function, step_size=0.001)
        # self.add(average_q)
        
        self.remove(gf, gq)
        number_of_functions_to_show = 10
        for i in range(number_of_functions_to_show):
            B = get_B_path(n)            
            f_t = lambda t: f(B(t))
            
            self.x_axis = x_axis1
            self.y_axis = y_axis1
            gf = self.get_graph(f_t, step_size=0.001)
            gf.make_jagged()

            self.x_axis = x_axis2
            self.y_axis = y_axis2
            q = get_quadratic_variation(f_t, B, n)
            gq = self.get_graph(q, step_size=0.001)
            gq.make_jagged()

            self.play(ShowCreation(gf), ShowCreation(gq), run_time=3, rate_func=linear)
            self.wait(2)
            if i < number_of_functions_to_show-1:
                self.remove(gf, gq)
        
        
        self.wait(10)

class StoppingTimeScene(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : 0,
        "y_max" : 1,
        "x_axis_width" : FRAME_WIDTH / 2 - 1,
        "y_axis_height": FRAME_HEIGHT / 2,
        "graph_origin" : (1.4+FRAME_HEIGHT/4)*DOWN, 
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "y_axis_label": "$\Psi_t$",
        "x_axis_label": "$t$",
    }
        
    def construct(self):        
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)
        
        tal_conj = TexMobject(r"\text{Conj: }",
                              r"\mathbb{E}\sqrt{h_f}",
                              r"\geq",
                              r"\mathrm{Var}[f]",
                              r"\sqrt{\log \left(1+\frac{e}{ \sum_i \mathrm{Inf}_i(f)^2} \right)}")
        tal_conj.set_color_by_tex("Var", RED_C)
        tal_conj.set_color_by_tex(r"\mathbb{E}\sqrt{h_f}", YELLOW_C)
        tal_conj.set_color_by_tex(r"log", GREEN_C)

        tal_conj.shift(2*UP)
        tal_conj.to_edge(LEFT)
        
        self.add(tal_conj)

        t1 = TexMobject(r"\mathbb{E}\sqrt{h_f}", r"=")
        t2 = TexMobject(r"\mathbb{E}\sqrt{\sum_{i=1}^n \partial_i(f)^2}")
        t3 = TexMobject(r"\mathbb{E}\sqrt{|| \nabla f ||^2}")
        t4 = TexMobject(r"\mathbb{E}|| \nabla f ||")
        t5 = TexMobject(r"\mathbb{E}|| \nabla f(B_1) ||")

        t6 = TexMobject(r"\Psi_t = || \nabla f(B_t) ||")
        t7 = TexMobject(r"\mathbb{E}\Psi_1")
        t8 = TexMobject(r"\mathbb{P}[\Psi_t \text{ large for }t<1] \text{ large?}")

        t1.set_color_by_tex("sqrt", YELLOW_C)
        t6.set_color(TEAL_C)
        t7.set_color(TEAL_C)
        
        t1.to_edge(LEFT)
        t2.next_to(t1, RIGHT)
        t3.next_to(t1, RIGHT)
        t4.next_to(t1, RIGHT)
        t5.next_to(t1, RIGHT)        

        t6.next_to(t1, DOWN)
        t6.shift(DOWN)
        t6.to_edge(LEFT)
        t7.next_to(tal_conj[2], LEFT)

        t8.next_to(t6, DOWN)
        t8.align_to(t6, LEFT)
        t8.shift(0.5*DOWN)
        
        
        self.wait(3)
        self.play(TransformFromCopy(tal_conj[1], t1))
        self.wait(1)
        self.play(Write(t2))
        self.wait(1)
        self.play(ReplacementTransform(t2, t3))
        self.wait(1)
        self.play(ReplacementTransform(t3, t4))
        self.wait(1)
        self.play(ReplacementTransform(t4, t5))
        self.wait(1)
        self.play(Write(t6))
        self.wait(1)
        self.play(TransformFromCopy(t6, t7), FadeOut(tal_conj[1]))
        self.wait(1)
        
        self.setup_axes(animate=False)
        self.remove(self.x_axis, self.y_axis)
        self.y_axis_label_mob.shift(0.5*DOWN)
        self.play(Write(VGroup(self.x_axis, self.y_axis)), Write(t8))

        
        l = Line(self.y_axis.get_center(), self.y_axis.get_center() + RIGHT*(FRAME_WIDTH/2-1))
        l.set_color(GOLD_C)
        l.shift(0.75*FRAME_HEIGHT/2*UP)

        alpha_t = TexMobject(r"\alpha")
        alpha_t.next_to(l, 0)
        alpha_t.shift(0.25*UP)
        alpha_t.set_color(RED_C)

        self.wait(1)
        self.play(ShowCreation(l), Write(alpha_t))
        
        n = 7
        f = majority
        derivatives = [get_harmonic_extension(get_derivative(f,i),n) for i in range(n)]
        scale_factor = 0.3
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]
        np.random.seed(675)
        B = get_B_path(n) # The next ones in this seed are better.
        number_of_functions_to_show = 3
        for i in range(number_of_functions_to_show):
            B = get_B_path(n)
            h = lambda t: scale_factor * sum([d(B(t))**2 for d in derivatives])            
            gh = self.get_graph(h, step_size=0.005, color=colors[i % len(colors)])
            gh.make_jagged()
            self.play(ShowCreation(gh), run_time=3, rate_func=linear)

        s1 = TexMobject(r"\tau_\alpha = \inf \{ t \mid \Psi_t > \alpha \} \land 1")
        s2 = TexMobject(r"\mathbb{E}\Psi_1", r"\geq", r"\mathbb{E}\Psi_{\tau_\alpha}")
        s3 = TexMobject(r"\geq", r"\mathbb{P}[\tau_\alpha < 1]", r"\alpha")
            
        s1.to_edge(LEFT)
        s1.shift(DOWN)

        s2.next_to(s1, DOWN)
        s2.to_edge(LEFT)
        s2.shift(DOWN)
        s3.next_to(s2, RIGHT)

        s2.set_color_by_tex("Psi", TEAL_C)
        s3.set_color_by_tex("alpha", GREEN_C)
        s3.set_color_by_tex("tau", RED_C)        
        
        to_remove = [t1,t5]                
        self.play(FadeOut(t1), FadeOut(t5), FadeOut(t8), t6.shift, 2*UP)
        self.play(Write(s1))
        self.wait(1)
        self.play(Write(s2), Write(s3))    
        self.wait(10)


class AnalyticToolsScene(GraphScene):
    CONFIG = {
        "x_min" : 0,
        "x_max" : 1,
        "y_min" : 0,
        "y_max" : 1,
        "x_axis_width" : FRAME_WIDTH / 2 - 1,
        "y_axis_height": FRAME_HEIGHT / 2,
        "graph_origin" : (1.4+FRAME_HEIGHT/4)*DOWN+ (FRAME_WIDTH/2-0.5)*LEFT,  
        "axes_color" : LIGHT_GRAY,
        "x_labeled_nums" :[0,1],
        "y_labeled_nums" : [-1,0,1],
        "y_tick_frequency": 0.2,
        "x_axis_label": "$t$",
    }
    
    def construct(self):
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)
        self.wait(3)
        
        t1 = TexMobject(r"\text{Level 2 ineq: }", r"\frac{\mathrm{d}}{\mathrm{d}t}\sum_i (\partial_i f_t)^2  ~\leq", r"~C(t)~||\nabla f_t||_2^2 \cdot \log \left(\frac{C(t)}{||\nabla f_t||_2^2} \right)")

        t1.shift(3*UP)
        
        self.play(Write(t1[0]))
        self.wait(1)
        self.play(Write(t1[1]))
        self.wait(1)
        self.play(Write(t1[2]))


        self.y_axis_label = r"$\Psi_t$"
        self.setup_axes(animate=False)
        self.remove(self.x_axis, self.y_axis)
        self.y_axis_label_mob.shift(0.5*DOWN)
        self.play(Write(VGroup(self.x_axis, self.y_axis)))

        l = Line(self.y_axis.get_center(), self.y_axis.get_center() + RIGHT*(FRAME_WIDTH/2-1))
        l.set_color(GOLD_C)
        l.shift(0.75*FRAME_HEIGHT/2*UP)

        alpha_t = TexMobject(r"\alpha")
        alpha_t.next_to(l, 0)
        alpha_t.shift(0.25*UP)
        alpha_t.set_color(RED_C)        
        
        self.play(ShowCreation(l), Write(alpha_t))
        self.wait(2)

        n = 7
        f = majority
        harmonic_f = get_harmonic_extension(f, n)
        derivatives = [get_harmonic_extension(get_derivative(f,i),n) for i in range(n)]
        scale_factor = 0.3
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]
        np.random.seed(675)
        
        B = get_B_path(n)
        h = lambda t: scale_factor * sum([d(B(t))**2 for d in derivatives])
        f_t = lambda t: harmonic_f(B(t))
        gh = self.get_graph(h, step_size=0.005, color=RED_C)
        gh.make_jagged()

        self.play(ShowCreation(gh), run_time=3, rate_func=linear)
        self.wait(2)

        
        x_intersect = 0.915
        vert_line = self.get_vertical_line_to_graph(x_intersect, gh,color=YELLOW)

        self.play(ShowCreation(vert_line))
        self.wait(2)

        t2 = TexMobject(r"t = 1 - 1/ \log(\ldots)")
        t2.align_to(vert_line, RIGHT+DOWN)
        t2.shift(0.1*UP + 0.1*LEFT)        
        self.play(Write(t2))
        self.wait(2)
        
        x_axis1 = self.x_axis
        y_axis1 = self.y_axis

        self.y_axis_label = r"$[f]_t$"
        self.graph_origin = (1.4+FRAME_HEIGHT/4)*DOWN+ 0.5*RIGHT;
        self.setup_axes(animate=False)
        self.remove(self.x_axis, self.y_axis)
        self.y_axis_label_mob.shift(0.5*DOWN)
        self.play(Write(VGroup(self.x_axis, self.y_axis)))        

        self.wait(1)
        
        q = get_quadratic_variation(f_t, B, n)        
        gq = self.get_graph(lambda t: 0.3*q(t), step_size=0.001)
        gq.make_jagged()
        self.play(ShowCreation(gq), run_time=3, rate_func=linear)
        

        self.wait(10)
        

class FinalSlideScene(Scene):
    
    def construct(self):
        # v = Line(TOP, BOTTOM, color=DARK_GRAY)
        # self.add(v)
        # v = Line(LEFT_SIDE, RIGHT_SIDE, color=DARK_GRAY)
        # self.add(v)
        
        the_end = TextMobject("The end, for now.")
        funding = TextMobject("Supported by ERC StG, ISF 718/19, Adams fellowship")
        # music = TextMobject("Music: Impulse / Ben Prunty")
        animation = TextMobject("Made with manim - math animation for python")
        bits = TextMobject("No bits were harmed in the making of this video")
        re = TextMobject("Ronen Eldan")
        rg = TextMobject("Renan Gross")
        
        h = Hypercube(shift=0.3*UP)

        funding.scale(0.5)
        # music.scale(0.5)
        animation.scale(0.5)
        bits.scale(0.5)
        
        the_end.shift(3*UP)
        funding.shift(2.1*DOWN)
        # music.next_to(funding, DOWN)
        animation.next_to(funding, DOWN)
        bits.next_to(animation, DOWN)

        re.shift(5*LEFT+1.25*DOWN)
        rg.shift(5*RIGHT+1.25*DOWN)

        self.play(GrowFromCenter(the_end),
                  ShowCreation(funding),
                  # GrowFromCenter(music),
                  FadeIn(animation),
                  ShowCreation(bits),
                  Write(re),
                  Write(rg))

        self.play(*[GrowFromCenter(x) for x in h.get_sorted_drawing()])
        
        # self.add(re, rg)        
        # self.add(the_end)
        # self.add(funding)
        # self.add(music)
        # self.add(animation)

        # self.add(*h.get_sorted_drawing())

        number_of_repeats = 10
        function_indicator = []
        colors = [YELLOW, RED_C, BLUE_C, GREEN_C, PINK]
        for i in range(number_of_repeats):
            old_function_indicator = function_indicator
            function_indicator = random.sample(h.nodes, 8)
            decay_animations = list(itertools.chain.from_iterable((h.nodes_to_draw[node].set_fill, DARK_GRAY) for node in old_function_indicator))
            if decay_animations:
                self.play(*decay_animations)
            light_animations = list(itertools.chain.from_iterable((h.nodes_to_draw[node].set_fill, colors[i % len(colors)]) for node in function_indicator))
            self.play(*light_animations)

        self.wait(3)
